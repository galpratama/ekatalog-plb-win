<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
            $table->increments('id');
			$table->string('name')->unique();
			$table->string('full_name');
			$table->string('email');
			$table->string('password');
			$table->string('phone');
			$table->string('picture');
            $table->boolean('full_screen');
            $table->boolean('push_notification');
            $table->boolean('status')->default(true);
            $table->string('remember_token', 100)->nullable();
            $table->string('role')->default('user');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
