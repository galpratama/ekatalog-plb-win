<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('items', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('categories_id');
            $table->string('title');
            $table->text('description');
            $table->string('picture'); // picture link
            $table->string('document'); // document (pdf) link
            $table->boolean('downloadable')->default(false);
            $table->boolean('status')->default(true);
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('items');
	}

}
