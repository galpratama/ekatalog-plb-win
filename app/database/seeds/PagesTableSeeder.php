<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class PagesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 1) as $index)
		{
			Page::create([
                'title' => "Profil Puslitbang",
                'description' => $faker->text(200)
			]);
		}
	}

}