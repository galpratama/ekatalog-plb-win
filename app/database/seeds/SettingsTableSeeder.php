<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class SettingsTableSeeder extends Seeder {

	public function run()
	{
		Setting::create([
			'playstore_link' => 'http://play.google.com/',
			'address' => '<p>Place Address Here</p>'
		]);
	}

}