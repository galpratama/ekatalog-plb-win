<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ItemsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 30) as $index)
		{
			Item::create([
                'categories_id' => $faker->numberBetween(1,12),
                'title' => $faker->sentence(6),
                'description' => $faker->text(200),
                'picture' => 'dummy.png',
                'document' => 'dummy.pdf',
                'downloadable' => $faker->boolean(50),
			]);
		}
	}

}