<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

        // Truncate all data in users before seeding
        User::truncate();
        Page::truncate();
        Faq::truncate();
        Contact::truncate();
        Category::truncate();
        Item::truncate();
        Bookmark::truncate();
        Favorite::truncate();
        Link::truncate();
        Setting::truncate();

		Eloquent::unguard();

        $this->call('UsersTableSeeder');
        $this->call('PagesTableSeeder');
//        $this->call('ContactsTableSeeder');
//        $this->call('FaqsTableSeeder');
//        $this->call('CategoriesTableSeeder');
//        $this->call('ItemsTableSeeder');
//        $this->call('BookmarksTableSeeder');
//        $this->call('FavoritesTableSeeder');
        $this->call('SettingsTableSeeder');


    }

}
