<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class FavoritesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 20) as $index)
		{
			Favorite::create([
                'items_id' => $faker->numberBetween(1,30),
                'users_id' => $faker->numberBetween(1,30)
			]);
		}
	}

}