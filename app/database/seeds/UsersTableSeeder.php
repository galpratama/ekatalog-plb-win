<?php

use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{

    public function run()
    {

//        $faker = Faker::create();
//
//        // Generate dummy data
//        foreach(range(1,30) as $index)
//        {
//
//            User::create([
//
//                'name' => $faker->userName,
//                'full_name' => $faker->name,
//                'email' => $faker->email,
//                'password' => Hash::make('password'),
//                'phone' => $faker->phoneNumber,
//                'picture' => 'dummy.png'
//
//            ]);
//
//        }

        User::create([

            'name' => 'pinkadmin',
            'full_name' => 'Administrator',
            'email' => 'admin@main.com',
            'password' => Hash::make('pinkpanda'),
            'phone' => '00000',
            'picture' => 'dummy.png',
            'role' => 'admin'

        ]);
    }
}