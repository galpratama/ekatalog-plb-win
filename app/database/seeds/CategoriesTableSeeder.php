<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CategoriesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 12) as $index)
		{
			Category::create([
                'title' => $faker->sentence(2),
                'description' => $faker->text(200),
                'picture' => 'dummy.png'
			]);
		}
	}

}