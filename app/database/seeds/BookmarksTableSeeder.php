<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class BookmarksTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 40) as $index)
		{
			Bookmark::create([
                'items_id' => $faker->numberBetween(1,30),
                'users_id' => $faker->numberBetween(1,30),
                'page_number' => $faker->numberBetween(1,10),
			]);
		}
	}

}