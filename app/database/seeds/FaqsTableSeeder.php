<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class FaqsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			Faq::create([
                'title' => $faker->sentence(6),
                'description' => $faker->text(200)
			]);
		}
	}

}