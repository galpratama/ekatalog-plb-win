<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::pattern('id', '[0-9]+');

// Frontend Routes
Route::get('/', array('uses' => 'FrontendController@index'));

Route::get('faq', array('uses' => 'FrontendController@faq'));

Route::get('item/{id}', array('uses' => 'FrontendController@item'));
Route::get('item', function(){ return Redirect::to('/'); });

Route::get('page/{id}', array('uses' => 'FrontendController@page'));
Route::get('page', function(){ return Redirect::to('/'); });

Route::get('favorite/{id}', array('uses' => 'FrontendController@addFavorite'));
Route::get('favorite', array('uses' => 'FrontendController@favorite'));

Route::get('category/{id}', array('uses' => 'FrontendController@categoryItem'));
Route::get('category', array('uses' => 'FrontendController@category'));

Route::get('contact', array('uses' => 'FrontendController@contact'));
Route::post('contact', array('uses' => 'FrontendController@submitContact'));

Route::get('logout', array('uses' => 'FrontendController@logout'));

Route::post('login', array('uses' => 'FrontendController@login'));

Route::post('register', array('uses' => 'FrontendController@register'));

Route::post('profile', array('uses' => 'FrontendController@profile'));


/* This code is used for auth using stateless HTTP Auth */
//  Route::group(['prefix' => 'v1', 'before' => 'auth.api'], function()

Route::group(['prefix' => 'api/v1'], function()
{
    // User API
    Route::resource('user','UserController',['except' => ['index','create','edit','destroy']]);

    // Items API
    Route::resource('item','ItemsController',['except' => ['create','edit','store','update','destroy']]);

    // Category API
    Route::resource('category','CategoriesController',['except' => ['create','edit','store','update','destroy']]);
    Route::get('category/{id}/item','CategoriesController@item');

    // Faq API
    Route::resource('faq','FaqController',['except' => ['create','edit','store','update','destroy']]);

    // Page API
    Route::resource('page','PageController',['except' => ['create','edit','store','update','destroy']]);

    // Contact API
    Route::resource('contact','ContactController',['except' => ['index','show','create','edit','update','destroy']]);

    // Favorite API
    Route::resource('favorite', 'FavoritesController',['except' => ['index','create','edit','update']]);

    // Bookmark API
    Route::resource('bookmark', 'BookmarksController',['except' => ['index','create','edit']]);

    // Auth API
    Route::post('auth',['as' => 'api.v1.auth.check', 'uses' => 'AuthController@show']);

});

// User and Admin Auth

    // route to show the admin login form
    Route::get('back-login', array('uses' => 'AdminAuthController@showLogin'));

    // route to process the admin login form
    Route::post('back-login', array('uses' => 'AdminAuthController@doLogin'));


Route::group(['prefix' => 'backend','before' => 'auth.admin'], function()
{
    // Backend Logout
    Route::get('logout', array('uses' => 'AdminAuthController@doLogout'));

    // Home Backend
    Route::get('/', function() {
        return View::make('backend.pages.home');
    });

    // User Backend
    Route::resource('user', 'BackendUserController', ['except' => ['show','create','edit','destroy']]);

    // Admin Backend
    Route::resource('administrator', 'BackendAdminController', ['except' => ['show','create','edit','destroy']]);

    // Items Backend
    Route::resource('item', 'BackendItemsController', ['except' => ['create','edit']]);

    // Category Backend
    Route::resource('category', 'BackendCategoriesController', ['except' => ['show','create','edit']]);

    // Faq Backend
    Route::resource('faq', 'BackendFaqController', ['except' => ['show','create','edit']]);

    // Page Backend
    Route::resource('page', 'BackendPageController', ['except' => ['show','create','edit']]);

    // Link Backend
    Route::resource('link', 'BackendLinkController', ['except' => ['show','create','edit']]);

    // Contact Backend
    Route::resource('contact', 'BackendContactController', ['except' => ['show','create','edit']]);

    // Setting Backend
    Route::resource('setting', 'BackendSettingsController', ['except' => ['show','create','edit','destroy']]);


});