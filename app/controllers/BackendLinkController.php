<?php

class BackendLinkController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /backendlink
	 *
	 * @return Response
	 */
	public function index()
	{
		$link = Link::all();
		return View::make('backend.pages.link')->with('link', $link);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /backendlink
	 *
	 * @return Response
	 */
	public function store()
	{
		// validate
		// read more on validation at http://laravel.com/docs/validation
		$rules = array(
			'title'       => 'required',
			'url'      => 'required',
		);
		$validator = Validator::make(Input::all(), $rules);

		// process validation
		if ($validator->fails())
		{
			return Redirect::to('backend/link')
				->withErrors($validator);
		}
		else
		{
			// store
			$link = new Link;
			$link->title       = Input::get('title');
			$link->url      = Input::get('url');
			$link->save();

			// redirect
			Session::flash('message', 'Tautan berhasil ditambahkan!');
			return Redirect::to('backend/link');
		}
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /backendlink/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// validate
		// read more on validation at http://laravel.com/docs/validation
		$rules = array(
			'title'       => 'required',
			'url'      => 'required',
		);
		$validator = Validator::make(Input::all(), $rules);

		// process validation
		if ($validator->fails())
		{
			return Redirect::to('backend/link')
				->withErrors($validator);
		}
		else
		{
			// store
			$link = Link::find($id);
			$link->title       	= Input::get('title');
			$link->url			= Input::get('url');
			$link->save();

			// redirect
			Session::flash('message', 'Tautan berhasil diperbarui!');
			return Redirect::to('backend/link');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /backendlink/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		// delete
		$link = Link::find($id);
		$link->delete();

		// redirect
		Session::flash('message', 'Tautan berhasil dihapus');
		return Redirect::to('backend/link');
	}

}