<?php

class BookmarksController extends \BaseController {


    /**
     * Store a newly created resource in storage.
     * POST /user
     *
     * @return Response
     */
    public function store()
    {
        // Validation rules
        $rules = array(
            'items_id'       => 'required',
            'users_id'  => 'required',
            'page_number'      => 'required'
        );

        // Process the validation rules
        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails())
        {

            return Response::json([
                'error' => [
                    'message' => 'There is an error.'
                ],
                'data' => $validator->messages()->toArray()
            ],500);
        }
        else
        {
            // Store
            $bookmark = new Bookmark;

            $bookmark->items_id      = Input::get('items_id');
            $bookmark->users_id      = Input::get('users_id');
            $bookmark->page_number      = Input::get('page_number');

            $bookmark->save();

            return Response::json([
                'success' => [
                    'message' => 'Bookmark successfully added.'
                ],
                'data' => [
                    'items_id' => Input::get('items_id'),
                    'users_id' => Input::get('users_id'),
                    'page_number' => Input::get('page_number')
                ]
            ], 201);

        }
    }


    /**
	 * Display the specified resource.
	 * GET /bookmarks/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

        if (Input::get('limit'))
        {
            $bookmark = Bookmark::where('users_id',$id)
                        ->paginate(Input::get('limit'));
        }
        else
        {
            $bookmark = Bookmark::where('users_id',$id)
                        ->paginate(5);
        }

        $data = $bookmark->toArray();

        if( ! $data )
        {
            return Response::json([
                'error' => [
                    'message' => 'There is an error.'
                ],
                'data' => [
                    'name' => 'This user doesn\'t have bookmarks or user doesn\'t exist'
                ]
            ], 500);
        }
        else
        {
            return Response::json([
                'data' => $data['data'],
                'paginator'=> [
                    "total_count" => $data['total'],
                    "per_page" => $data['per_page'],
                    "current_page" => $data['current_page'],
                    "total_pages" => $data['last_page']
                ]
            ], 200);

        }
	}


	/**
	 * Update the specified resource in storage.
	 * PUT /bookmarks/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $bookmark = [];

        // Prepare field
        if(Input::has('page_number')) $bookmark['page_number'] = Input::get('page_number');

        // Update
        Bookmark::where('items_id', Input::get('items_id'))
                ->where('users_id', $id)
                ->update($bookmark);

        // Load inserted data
        $check = Bookmark::where('items_id', Input::get('items_id'))
                ->where('users_id', $id)
                ->get();

        return Response::json([
            'success' => [
                'message' => 'Bookmark successfully updated.'
            ],
            'data' => [
                'id' => $check->toArray()
            ]
        ], 200);
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /bookmarks/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        Bookmark::where('items_id', Input::get('items_id'))
            ->where('users_id', $id)
            ->delete();

        return Response::json([
            'success' => [
                'message' => 'Bookmark successfully deleted.'
            ],
            'data' => [
                'items_id' => Input::get('items_id'),
                'users_id' => $id
            ]
        ], 200);
	}

}