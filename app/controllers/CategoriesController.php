<?php

class CategoriesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /category
	 *
	 * @return Response
	 */
	public function index()
	{
        if (Input::get('limit'))
        {
            $category = Category::paginate(Input::get('limit'));
        }
        else
        {
            $category = Category::paginate(5);
        }

        $data = $category->toArray();
        return Response::json([
            'data' => $data['data'],
            'paginator'=> [
                "total_count" => $data['total'],
                "per_page" => $data['per_page'],
                "current_page" => $data['current_page'],
                "total_pages" => $data['last_page']
            ]
        ], 200);
	}

	/**
	 * Display the specified resource.
	 * GET /category/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $category = Category::find($id);

        if( ! $category )
        {
            return Response::json([
                'error' => [
                    'message' => 'There is an error.'
                ],
                'data' => [
                    'name' => 'Categories does not exist.'
                ]
            ], 500);
        }
        else
        {
            return Response::json([
                'data' => $category->toArray()
            ], 200);

        }
	}

    /**
     * Show items inside category
     * GET /category/{id}/item
     *
     * @param  int  $id
     * @return Response
     */

    public function item($id)
    {
        $item = Item::where('categories_id', $id)->get();

        if( ! $item )
        {
            return Response::json([
                'error' => [
                    'message' => 'There is an error.'
                ],
                'data' => [
                    'name' => 'Page does not exist.'
                ]
            ], 500);
        }
        else
        {
            return Response::json([
                'data' => $item->toArray()
            ], 200);

        }
    }

	/**
	 * Show the form for editing the specified resource.
	 * GET /category/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /category/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /category/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}