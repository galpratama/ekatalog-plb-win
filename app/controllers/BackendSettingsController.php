<?php

class BackendSettingsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /backendsettings
	 *
	 * @return Response
	 */
	public function index()
	{
		$setting = Setting::find(1);
		return View::make('backend.pages.setting')->with('setting', $setting);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /backendsettings/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// validate
		// read more on validation at http://laravel.com/docs/validation
		$rules = array(
			'playstore_link'    => 'required',
			'address'     		=> 'required',
		);
		$validator = Validator::make(Input::all(), $rules);

		// process validation
		if ($validator->fails())
		{
			return Redirect::to('backend/setting')
				->withErrors($validator);
		}
		else
		{
			// store
			$setting = Setting::find($id);
			$setting->playstore_link       	= Input::get('playstore_link');
			$setting->address   			= Input::get('address');
			$setting->save();

			// redirect
			Session::flash('message', 'Pengaturan berhasil diperbarui!');
			return Redirect::to('backend/setting');
		}
	}


}