<?php

class UserController extends \BaseController {

	/**
	 * Store a newly created resource in storage.
	 * POST /user
	 *
	 * @return Response
	 */
	public function store()
	{
		// Validation rules
        $rules = array(
            'name'       => 'required',
            'full_name'  => 'required',
            'email'      => 'required|email',
            'password'   => 'required',
            'phone'      => 'required'
        );

        // Process the validation rules
        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails())
        {

            return Response::json([
                'error' => [
                    'message' => 'There is an error.'
                ],
                'data' => $validator->messages()->toArray()
            ],500);
        }
        else
        {
            // Store
            $user = new User;

            $user->name      = Input::get('name');
            $user->password  = Hash::make( base64_decode( Input::get('password') ) );
            $user->full_name = Input::get('full_name');
            $user->email     = Input::get('email');
            $user->phone     = Input::get('phone');

            try
            {
                $user->save();

                return Response::json([
                    'success' => [
                        'message' => 'User successfully added.'
                    ],
                    'data' => [
                        'name' => Input::get('name'),
                        'full_name' => Input::get('full_name')
                    ]
                ], 201);

            }
            catch(\Exception $error)
            {
                if ( $error->errorInfo[1] === 1062 ) // if query returned duplicate entry error
                {
                    return Response::json([
                        'error' => [
                            'message' => 'There is an error.',
                        ],
                        'data' => [
                            'name' => 'User \'' . Input::get('name') . '\' already exist.'
                        ]
                    ],400);
                }

            }

        }
	}

	/**
	 * Display the specified resource.
	 * GET /user/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

        if (is_numeric($id))
        {
            // If input is numeric

            $user = User::find($id);
        }
        else
        {
            // if input is string

            $user = User::where('name' , '=', $id)->first();
        }

        if($id == 'auth')
        {
            // if url is contain 'auth'

            return Response::json([
                'error' => [
                    'message' => 'Auth error.'
                ],
                'data' => [
                    'name' => 'You need to specify name.'
                ]
            ], 400);
        }
        else if( ! $user )
        {

            // if there is no user in database

            return Response::json([
                'error' => [
                    'message' => 'There is an error.'
                ],
                'data' => [
                    'name' => 'User does not exist.'
                ]
            ], 400);
        }
        else
        {
            // if there is user in database and url is not contain 'auth'

            return Response::json([
                'data' => $user->toArray()
            ], 200);

        }

	}

	/**
	 * Update the specified resource in storage.
	 * PUT /user/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        // Validation rules
        $rules = array(
            'email'      => 'email',
        );

        // Process the validation rules
        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails())
        {
            return Response::json([
                'error' => [
                    'message' => 'There is an error.'
                ],
                'data' => $validator->messages()->toArray()
            ],500);
        }
        else
        {
            // Store
            $user = User::find($id);

            // Update if exist
            if(Input::has('name'))                  $user->name                 = Input::get('name');
            if(Input::has('password'))              $user->password             = Hash::make(base64_decode(Input::get('password')));
            if(Input::has('full_name'))             $user->full_name            = Input::get('full_name');
            if(Input::has('email'))                 $user->email                = Input::get('email');
            if(Input::has('phone'))                 $user->phone                = Input::get('phone');
            if(Input::has('picture'))               $user->picture              = Input::get('picture');
            if(Input::has('full_screen')) {         $user->full_screen          = Input::get('full_screen') == "1" ? 1 : 0; }
            if(Input::has('push_notification')) {   $user->push_notification    = Input::get('push_notification') == "1" ? 1 : 0;}

            $user->save();

            return Response::json([
                'success' => [
                    'message' => 'User successfully updated.'
                ],
                'data' => [
                    'id' => $id
                ]
            ], 200);


        }
	}

}
