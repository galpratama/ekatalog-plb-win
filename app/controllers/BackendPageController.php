<?php

class BackendPageController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /backendpage
	 *
	 * @return Response
	 */
	public function index()
	{
        $page = Page::all();
        return View::make('backend.pages.page')->with('page', $page);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /backendpage
	 *
	 * @return Response
	 */
	public function store()
	{
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'title'       => 'required',
            'description'      => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process validation
        if ($validator->fails())
        {
            return Redirect::to('backend/page')
                ->withErrors($validator);
        }
        else
        {
            // store
            $page = new Page;
            $page->title       = Input::get('title');
            $page->description      = Input::get('description');
            $page->save();

            // redirect
            Session::flash('message', 'Halaman berhasil ditambahkan!');
            return Redirect::to('backend/page');
        }
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /backendpage/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'title'       => 'required',
            'description'      => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process validation
        if ($validator->fails())
        {
            return Redirect::to('backend/page')
                ->withErrors($validator);
        }
        else
        {
            // store
            $page = Page::find($id);
            $page->title       = Input::get('title');
            $page->description      = Input::get('description');
            $page->save();

            // redirect
            Session::flash('message', 'Halaman berhasil diperbarui!');
            return Redirect::to('backend/page');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /backendpage/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        // delete
        $page = Page::find($id);
        $page->delete();

        // redirect
        Session::flash('message', 'Halaman berhasil dihapus');
        return Redirect::to('backend/page');
	}

}