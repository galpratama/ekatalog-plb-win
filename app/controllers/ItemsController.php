<?php

class ItemsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /items
	 *
	 * @return Response
	 */
	public function index()
	{
        if (Input::get('limit'))
        {
            $item = Item::paginate(Input::get('limit'));
        }
        else
        {
            $item = Item::paginate(5);
        }

        $data = $item->toArray();
        return Response::json([
            'data' => $data['data'],
            'paginator'=> [
                "total_count" => $data['total'],
                "per_page" => $data['per_page'],
                "current_page" => $data['current_page'],
                "total_pages" => $data['last_page']
            ]
        ], 200);
	}

	/**
	 * Display the specified resource.
	 * GET /items/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $item = Item::find($id);

        if( ! $item )
        {
            return Response::json([
                'error' => [
                    'message' => 'There is an error.'
                ],
                'data' => [
                    'name' => 'Page does not exist.'
                ]
            ], 500);
        }
        else
        {
            return Response::json([
                'data' => $item->toArray()
            ], 200);

        }
	}

}