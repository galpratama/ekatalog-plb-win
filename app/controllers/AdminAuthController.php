<?php

class AdminAuthController extends \BaseController {

    public function showLogin()
    {
        if (Auth::check())
        {
            return Redirect::to('backend');
        }

        else
        {
            return View::make('backend.login');
        }

    }

    public function doLogin()
    {
        // validate the info, create rules for the inputs
        $rules = array(
            'name' => 'required',
            // make sure the email is an actual email
            'password' => 'required|alphaNum|min:3'
            // password can only be alphanumeric and has to be greater than 3 characters
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::to('back-login')
                ->withErrors($validator)
                // send back all errors to the login form
                ->withInput(Input::except('password'));
            // send back the input (not the password) so that we can repopulate the form
        } else {

            // create our user data for the authentication
            $userdata = array(
                'name' => Input::get('name'),
                'password' => Input::get('password'),
                'role' => 'admin'
            );

            // attempt to do the login
            if (Auth::attempt($userdata)) {

                // validation successful!
                // redirect them administration page
                return Redirect::to('backend');

            } else {

                // validation not successful, send back to form
                return Redirect::to('back-login');

            }

        }
    }

    public function doLogout()
    {
        Auth::logout();
        // log the user out of our application
        return Redirect::to('back-login');
        // redirect the user to the login screen
    }

}