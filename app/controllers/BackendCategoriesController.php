<?php

class BackendCategoriesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /categories
	 *
	 * @return Response
	 */
	public function index()
	{
        $category = Category::all();
        return View::make('backend.pages.category')->with('category', $category);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /categories
	 *
	 * @return Response
	 */
	public function store()
	{
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'title'       => 'required',
            'description'      => 'required',
            'file'       => 'required|max:10000|mimes:jpg,jpeg,png,gif'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process validation
        if ($validator->fails())
        {
            return Redirect::to('backend/category')
                ->withErrors($validator);
        }
        else
        {
            // Upload file
            $file = Input::file('file');

            $destinationPath = public_path() . '/uploads/category';
            $filename = str_random(12) . '.' . $file->getClientOriginalExtension();

            $file->move($destinationPath, $filename);

            // store
            $category = new Category;
            $category->title            = Input::get('title');
            $category->description      = Input::get('description');
            $category->picture          = $filename;
            $category->save();

            // redirect
            Session::flash('message', 'Kategori Katalog berhasil ditambahkan!');
            return Redirect::to('backend/category');
        }
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /categories/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'title'       => 'required',
            'description'      => 'required',
            'file'       => 'max:10000|mimes:jpg,jpeg,png,gif'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process validation
        if ($validator->fails())
        {
            return Redirect::to('backend/category')
                ->withErrors($validator);
        }
        else
        {
            // store
            $category = Category::find($id);

            if( Input::hasFile('file') )
            {
                // Upload file
                $file = Input::file('file');

                $destinationPath = public_path() . '/uploads/category';
                $filename = str_random(12) . '.' . $file->getClientOriginalExtension();

                $file->move($destinationPath, $filename);

                $category->picture = $filename;
            }

            $category->title       = Input::get('title');
            $category->description      = Input::get('description');
            $category->save();

            // redirect
            Session::flash('message', 'Kategori Katalog berhasil diperbarui!');
            return Redirect::to('backend/category');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /categories/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        // delete
        $category = Category::find($id);
        $category->delete();

        // delete existing item on category
        Item::where("categories_id", $id)->delete();

        // redirect
        Session::flash('message', 'Kategori Katalog berhasil dihapus beserta isinya');
        return Redirect::to('backend/category');
	}

}