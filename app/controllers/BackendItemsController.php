<?php

class BackendItemsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /items
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /items
	 *
	 * @return Response
	 */
	public function store()
	{
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'title'       => 'required',
            'description'      => 'required',
            'file'       => 'required|max:10000|mimes:jpg,jpeg,png,gif',
            'docs'       => 'required|max:20000|mimes:pdf'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process validation
        if ($validator->fails())
        {
            return Redirect::to('backend/item/' . Input::get('categories_id'))
                ->withErrors($validator);
        }
        else
        {
            // Upload file
            $file = Input::file('file');

            $destinationPath = public_path() . '/uploads/item';
            $filename = str_random(12) . '.' . $file->getClientOriginalExtension();

            $file->move($destinationPath, $filename);

            // Upload docs
            $file_docs = Input::file('docs');

            $destinationPath_docs = public_path() . '/uploads/docs';
            $filename_docs = str_random(12) . '.' . $file_docs->getClientOriginalExtension();

            $file_docs->move($destinationPath_docs, $filename_docs);

            // store
            $item = new Item;
            $item->title                = Input::get('title');
            $item->description          = Input::get('description');
            $item->categories_id        = Input::get('categories_id');
            $item->picture              = $filename;
            $item->document             = $filename_docs;
            $item->save();

            // redirect
            Session::flash('message', 'Katalog berhasil ditambahkan!');
            return Redirect::to('backend/item/' . Input::get('categories_id'));
        }
	}

	/**
	 * Display the specified resource.
	 * GET /items/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $item = Item::where('categories_id', $id)->get();
        $category = Category::find($id);
        $category_dropdown = Category::all();
        return View::make('backend.pages.item')
                ->with('item', $item)
                ->with('category',$category)
                ->with('category_dropdown',$category_dropdown);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /items/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'title'       => 'required',
            'description'      => 'required',
            'file'       => 'max:10000|mimes:jpg,jpeg,png,gif',
            'docs'       => 'max:20000|mimes:pdf'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process validation
        if ($validator->fails())
        {
            return Redirect::to('backend/item/' . Input::get('categories_id'))
                ->withErrors($validator);
        }
        else
        {
            // store
            $item = Item::find($id);

            if( Input::hasFile('file') )
            {
                // Upload file
                $file_image = Input::file('file');

                $destinationPath = public_path() . '/uploads/item';
                $filename_image = str_random(12) . '.' . $file_image->getClientOriginalExtension();

                $file_image->move($destinationPath, $filename_image);

                $item->picture = $filename_image;
            }

            if( Input::hasFile('docs') )
            {
                // Upload file
                $file_docs = Input::file('docs');

                $destinationPath = public_path() . '/uploads/docs';
                $filename_docs = str_random(12) . '.' . $file_docs->getClientOriginalExtension();

                $file_docs->move($destinationPath, $filename_docs);

                $item->document = $filename_docs;
            }

            $item->title       = Input::get('title');
            $item->description      = Input::get('description');
            $item->categories_id      = Input::get('categories_id');
            $item->save();

            // redirect
            Session::flash('message', 'Katalog berhasil diperbarui!');
            return Redirect::to('backend/item/' . Input::get('categories_id'));
        }
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /items/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        // delete
        $item = Item::find($id);
        $item->delete();

        // delete existing item on favorites
        Favorite::where("items_id", $id)->delete();

        // delete existing item on bookmark
        Bookmark::where("items_id", $id)->delete();

        // redirect
        Session::flash('message', 'Katalog berhasil dihapus!');
        return Redirect::to('backend/item/' . Input::get('categories_id'));
	}

}