<?php

class PageController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /page
	 *
	 * @return Response
	 */
	public function index()
	{
        if (Input::get('limit'))
        {
            $page = Page::paginate(Input::get('limit'));
        }
        else
        {
            $page = Page::paginate(5);
        }

        $data = $page->toArray();
        return Response::json([
            'data' => $data['data'],
            'paginator'=> [
                "total_count" => $data['total'],
                "per_page" => $data['per_page'],
                "current_page" => $data['current_page'],
                "total_pages" => $data['last_page']
            ]
        ], 200);
	}

	/**
	 * Display the specified resource.
	 * GET /page/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $page = Page::find($id);

        if( ! $page )
        {
            return Response::json([
                'error' => [
                    'message' => 'There is an error.'
                ],
                'data' => [
                    'name' => 'Page does not exist.'
                ]
            ], 500);
        }
        else
        {
            return Response::json([
                'data' => $page->toArray()
            ], 200);

        }
	}

}