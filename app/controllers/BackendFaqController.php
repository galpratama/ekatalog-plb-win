<?php

class BackendFaqController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /backendfaq
	 *
	 * @return Response
	 */
	public function index()
	{
        $faq = Faq::all();
        return View::make('backend.pages.faq')->with('faq', $faq);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /backendfaq
	 *
	 * @return Response
	 */
	public function store()
	{
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'title'       => 'required',
            'description'      => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process validation
        if ($validator->fails())
        {
            return Redirect::to('backend/faq')
                ->withErrors($validator);
        }
        else
        {
            // store
            $faq = new Faq;
            $faq->title       = Input::get('title');
            $faq->description      = Input::get('description');
            $faq->save();

            // redirect
            Session::flash('message', 'FAQ berhasil ditambahkan!');
            return Redirect::to('backend/faq');
        }
    }

	/**
	 * Update the specified resource in storage.
	 * PUT /backendfaq/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'title'       => 'required',
            'description'      => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process validation
        if ($validator->fails())
        {
            return Redirect::to('backend/faq')
                ->withErrors($validator);
        }
        else
        {
            // store
            $faq = Faq::find($id);
            $faq->title       = Input::get('title');
            $faq->description      = Input::get('description');
            $faq->save();

            // redirect
            Session::flash('message', 'FAQ berhasil diperbarui!');
            return Redirect::to('backend/faq');
        }
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /backendfaq/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        // delete
        $faq = Faq::find($id);
        $faq->delete();

        // redirect
        Session::flash('message', 'FAQ berhasil dihapus');
        return Redirect::to('backend/faq');
	}

}