<?php

class BackendContactController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /backendcontact
	 *
	 * @return Response
	 */
	public function index()
	{
        $contact = Contact::all();
        return View::make('backend.pages.contact')->with('contact', $contact);
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /backendcontact/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        // delete
        $contact = Contact::find($id);
        $contact->delete();

        // redirect
        Session::flash('message', 'Pesan kontak berhasil dihapus');
        return Redirect::to('backend/contact');
	}

}