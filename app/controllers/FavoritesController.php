<?php

class FavoritesController extends \BaseController {


    /**
     * Store a newly created resource in storage.
     * POST /favorite
     *
     * @return Response
     */
    public function store()
    {
        // Validation rules
        $rules = array(
            'items_id'       => 'required',
            'users_id'  => 'required'
        );

        // Process the validation rules
        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails())
        {

            return Response::json([
                'error' => [
                    'message' => 'There is an error.'
                ],
                'data' => $validator->messages()->toArray()
            ],500);
        }
        else
        {
            // Store
            $favorite = new Favorite;

            $favorite->items_id      = Input::get('items_id');
            $favorite->users_id      = Input::get('users_id');

            $favorite->save();

            return Response::json([
                'success' => [
                    'message' => 'Favorite successfully added.'
                ],
                'data' => [
                    'items_id' => Input::get('items_id'),
                    'users_id' => Input::get('users_id')
                ]
            ], 201);

        }
    }


    /**
     * Display the specified resource.
     * GET /favorites/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

        if (Input::get('limit'))
        {
            $favorite = Favorite::where('users_id',$id)
                ->paginate(Input::get('limit'));
        }
        else
        {
            $favorite = Favorite::where('users_id',$id)
                ->paginate(5);
        }

        $data = $favorite->toArray();

        if( ! $data )
        {
            return Response::json([
                'error' => [
                    'message' => 'There is an error.'
                ],
                'data' => [
                    'name' => 'This user doesn\'t have favorites or user doesn\'t exist'
                ]
            ], 500);
        }
        else
        {
            return Response::json([
                'data' => $data['data'],
                'paginator'=> [
                    "total_count" => $data['total'],
                    "per_page" => $data['per_page'],
                    "current_page" => $data['current_page'],
                    "total_pages" => $data['last_page']
                ]
            ], 200);

        }
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /favorites/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Favorite::where('items_id', Input::get('items_id'))
            ->where('users_id', $id)
            ->delete();

        return Response::json([
            'success' => [
                'message' => 'Favorite successfully deleted.'
            ],
            'data' => [
                'items_id' => Input::get('items_id'),
                'users_id' => $id
            ]
        ], 200);
    }

}