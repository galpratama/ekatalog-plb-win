<?php

class FrontendController extends \BaseController {

    function index()
    {
        $item = Item::orderBy('id','desc')->paginate(12);
        return View::make('frontend.pages.home')->with('item', $item);
    }

    function faq()
    {
        $faq = Faq::all();
        return View::make('frontend.pages.faq')->with('faq', $faq);
    }

    function page($id)
    {
        $page = Page::find($id);
        return View::make('frontend.pages.page')
            ->with('page', $page);
    }

    function item($id)
    {
        $item = Item::findOrFail($id);
        $category = Category::where('id', $item->categories_id)->take(1)->get();

        if(Auth::check())
        {
            $favoriteNotExists = Favorite::where('items_id', $id)
                ->where('users_id', Auth::user()->id)
                ->exists();

            if ($favoriteNotExists)
            {
                $showFavoritesButton = false;
            }
            else
            {
                $showFavoritesButton = true;
            }

            return View::make('frontend.pages.item')
                ->with('item', $item)
                ->with('category', $category)
                ->with('favoriteNotExists', $showFavoritesButton);
        }
        else
        {
            return View::make('frontend.pages.item')
                ->with('item', $item)
                ->with('category', $category);
        }
    }

    function favorite()
    {
        if(Auth::check())
        {
            // Get user favorites
            $favorite = Favorite::where('users_id', Auth::user()->id)->lists('items_id');

            if ( is_null($favorite) )
            {
                return Redirect::to('/');
            }

            else
            {
                $item = Item::whereIn('id', $favorite)->orderBy('id','desc')->paginate(12);
                return View::make('frontend.pages.favorite')
                    ->with('item', $item);
            }

        }
        else
        {
            return Redirect::to('/');
        }

    }

    function addFavorite($id)
    {
        // Store
        $favorite = new Favorite;

        $favorite->items_id  = $id;
        $favorite->users_id  = Auth::user()->id;

        try
        {
            $favorite->save();

            // redirect
            Session::flash('message', 'Berhasil menambahkan ke Pustaka');
            return Redirect::to('/item' . "/" . $id);

        }
        catch(\Exception $error)
        {
            if ( $error->errorInfo[1] === 1062 ) // if query returned duplicate entry error
            {
                // redirect
                Session::flash('message', 'Maaf, pustaka katalog ini sudah ada!');
                return Redirect::to('/');
            }

        }
    }

    function contact()
    {
        return View::make('frontend.pages.contact');
    }


    public function submitContact()
    {
        // Validation rules
        $rules = array(
            'name'       => 'required',
            'email'      => 'required|email',
            'message'    => 'required'
        );

        // Process the validation rules
        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails())
        {

            return Redirect::to('/contact')
                ->withErrors($validator);
        }
        else
        {
            // Store
            $contact = new Contact;

            $contact->name      = Input::get('name');
            $contact->email     = Input::get('email');
            $contact->message     = Input::get('message');

            $contact->save();

            // redirect
            Session::flash('message', 'Terimakasih atas masukkan anda.');
            return Redirect::to('/contact');

        }
    }

    function category()
    {
        $category = Category::orderBy('id','desc')->paginate(8);
        return View::make('frontend.pages.category')->with('category', $category);
    }

    function categoryItem($id)
    {
        $item = Item::where('categories_id', $id)->orderBy('id','desc')->paginate(12);
        $category = Category::find($id);
        return View::make('frontend.pages.categoryItem')
            ->with('item', $item)
            ->with('category', $category);
    }

    public function login()
    {
        // validate the info, create rules for the inputs
        $rules = array(
            'name' => 'required',
            // make sure the email is an actual email
            'password' => 'required|alphaNum|min:3'
            // password can only be alphanumeric and has to be greater than 3 characters
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::to('back-login')
                ->withErrors($validator)
                // send back all errors to the login form
                ->withInput(Input::except('password'));
            // send back the input (not the password) so that we can repopulate the form
        } else {

            // create our user data for the authentication
            $userdata = array(
                'name' => Input::get('name'),
                'password' => Input::get('password'),
                'role' => 'user'
            );

            // attempt to do the login
            if (Auth::attempt($userdata)) {

                // validation successful!
                // redirect them to homepage
                return Redirect::to('/');

            } else {

                // validation not successful, redirect to homepage
                return Redirect::to('/');

            }

        }
    }

    public function register()
    {
        // Validation rules
        $rules = array(
            'name'       => 'required',
            'full_name'  => 'required',
            'email'      => 'required|email',
            'password'   => 'required|min:3|confirmed',
            'password_confirmation' => 'required|min:3',
            'phone'      => 'required',
            'file'       => 'required|max:10000|mimes:jpg,jpeg,png,gif'
        );

        // Process the validation rules
        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails())
        {

            return Redirect::to('backend/user')
                ->withErrors($validator);
        }
        else
        {
            // Upload file
            $file = Input::file('file');

            $destinationPath = public_path() . '/uploads/user';
            $filename = str_random(12) . '.' . $file->getClientOriginalExtension();

            $file->move($destinationPath, $filename);

            // Store
            $user = new User;

            $user->name      = Input::get('name');
            $user->password  = Hash::make( base64_decode( Input::get('password') ) );
            $user->full_name = Input::get('full_name');
            $user->email     = Input::get('email');
            $user->phone     = Input::get('phone');
            $user->picture   = $filename;

            try
            {
                $user->save();

                // redirect
                Session::flash('message', 'Selamat, anda berhasil terdaftar. Silahkan masuk dengan mengklik tombol "Masuk"');
                return Redirect::to('/');

            }
            catch(\Exception $error)
            {
                if ( $error->errorInfo[1] === 1062 ) // if query returned duplicate entry error
                {
                    // redirect
                    Session::flash('message', 'Maaf, pengguna dengan nama ' . Input::get('name') . ' sudah ada!');
                    return Redirect::to('/');
                }

            }

        }
    }

    public function profile()
    {
        // Validation rules
        $rules = array(
            'email'      => 'email',
        );

        // Process the validation rules
        $validator = Validator::make(Input::all(), $rules);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::to('/')
                ->withErrors($validator)
                // send back all errors to the login form
                ->withInput(Input::except('password'));
            // send back the input (not the password) so that we can repopulate the form
        }
        else
        {
            // Store
            $user = User::find(Auth::user()->id);

            // Update if exist
            if(Input::has('name'))                  $user->name                 = Input::get('name');
            if(Input::has('full_name'))             $user->full_name            = Input::get('full_name');
            if(Input::has('email'))                 $user->email                = Input::get('email');
            if(Input::has('phone'))                 $user->phone                = Input::get('phone');

            $user->save();

            // redirect
            Session::flash('message', 'Profil berhasil diubah!');
            return Redirect::to('/');

        }
    }


    public function logout()
    {
        Auth::logout();
        // log the user out of our application
        return Redirect::to('/');
        // redirect the user to the login screen
    }





}