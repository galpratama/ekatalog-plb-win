<?php

class AuthController extends \BaseController {

	/**
	 * Display the specified resource.
	 *
	 * @return Response
	 */
	public function show()
	{
        $user = User::where('name', '=', Input::get('name'))->first();

        if ( ! Input::has('name') )
        {
            return Response::json([
                'error' => [
                    'message' => 'Auth error.'
                ],
                'data' => [
                    'name' => 'You must specify name.',

                ]
            ], 400);
        }

        else if ( ! $user )
        {
            return Response::json([
                'error' => [
                    'message' => 'Auth error.'
                ],
                'data' => [
                    'name' => 'User \''. Input::get('name') .'\' does not exist.',

                ]
            ], 400);
        }

        else
        {
            if ( ! Input::has('password') )
            {
                return Response::json([
                    'error' => [
                        'message' => 'Auth failed.',
                    ],
                    'data' => [
                        'password' => 'You must specify a password.'
                    ]
                ],400);
            }
            else
            {
                if ( Hash::check( base64_decode(Input::get('password')) , $user->password ) )
                {
                    return Response::json([
                        'success' => [
                            'message' => 'Auth success.',
                        ],
                        'data' => $user->toArray()
                    ],200);
                }

                else
                {
                    return Response::json([
                        'error' => [
                            'message' => 'Auth failed.',
                        ],
                        'data' => [
                            'password' => 'Wrong password.'
                        ]
                    ],400);
                }
            }
        }
	}


}
