<?php

class FaqController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /faq
	 *
	 * @return Response
	 */
	public function index()
	{
        if (Input::get('limit'))
        {
            $faq = Faq::paginate(Input::get('limit'));
        }
        else
        {
            $faq = Faq::paginate(5);
        }

        $data = $faq->toArray();
        return Response::json([
            'data' => $data['data'],
            'paginator'=> [
                "total_count" => $data['total'],
                "per_page" => $data['per_page'],
                "current_page" => $data['current_page'],
                "total_pages" => $data['last_page']
            ]
        ], 200);
	}

	/**
	 * Display the specified resource.
	 * GET /faq/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $faq = Faq::find($id);

        if( ! $faq )
        {
            return Response::json([
                'error' => [
                    'message' => 'There is an error.'
                ],
                'data' => [
                    'name' => 'FAQ does not exist.'
                ]
            ], 500);
        }
        else
        {
            return Response::json([
                'data' => $faq->toArray()
            ], 200);

        }
	}

}