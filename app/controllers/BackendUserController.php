<?php

class BackendUserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /backenduser
	 *
	 * @return Response
	 */
	public function index()
	{
        $user = User::where('role', 'user')->orderBy('full_name','asc')->get();
        return View::make('backend.pages.user')->with('user', $user);
	}


	/**
	 * Store a newly created resource in storage.
	 * POST /backenduser
	 *
	 * @return Response
	 */
	public function store()
	{
        // Validation rules
        $rules = array(
            'name'       => 'required',
            'full_name'  => 'required',
            'email'      => 'required|email',
            'password'   => 'required',
            'phone'      => 'required',
            'file'       => 'required|max:10000|mimes:jpg,jpeg,png,gif'
        );

        // Process the validation rules
        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails())
        {

            return Redirect::to('backend/user')
                ->withErrors($validator);
        }
        else
        {
            // Upload file
            $file = Input::file('file');

            $destinationPath = public_path() . '/uploads/user';
            $filename = str_random(12) . '.' . $file->getClientOriginalExtension();

            $file->move($destinationPath, $filename);

            // Store
            $user = new User;

            $user->name      = Input::get('name');
            $user->password  = Hash::make( base64_decode( Input::get('password') ) );
            $user->full_name = Input::get('full_name');
            $user->email     = Input::get('email');
            $user->phone     = Input::get('phone');
            $user->picture   = $filename;

            try
            {
                $user->save();

                // redirect
                Session::flash('message', 'Pengguna berhasil ditambahkan!');
                return Redirect::to('backend/user');

            }
            catch(\Exception $error)
            {
                if ( $error->errorInfo[1] === 1062 ) // if query returned duplicate entry error
                {
                    // redirect
                    Session::flash('message', 'Maaf, pengguna dengan nama ' . Input::get('name') . ' sudah ada! Data tidak dimasukkan.');
                    return Redirect::to('backend/user');
                }

            }

        }
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /backenduser/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        // Validation rules
        $rules = array(
            'name'       => 'required',
            'full_name'  => 'required',
            'email'      => 'required|email',
            'phone'      => 'required'
        );

        // Process the validation rules
        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails())
        {

            return Redirect::to('backend/user')
                ->withErrors($validator);
        }
        else
        {
            // Store
            $user = User::find($id);

            $user->name      = Input::get('name');
            $user->full_name = Input::get('full_name');
            $user->email     = Input::get('email');
            $user->phone     = Input::get('phone');

            $user->save();

            // redirect
            Session::flash('message', 'Data Pengguna berhasil diubah!');
            return Redirect::to('backend/user');

        }
	}


}