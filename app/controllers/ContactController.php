<?php

class ContactController extends \BaseController {

	/**
	 * Store a newly created resource in storage.
	 * POST /contact
	 *
	 * @return Response
	 */
	public function store()
	{
        return Response::json([
            'error' => [
                'message' => 'Nothing to do here.'
            ]
        ], 500);
	}



}