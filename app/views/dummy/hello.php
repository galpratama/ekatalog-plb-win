<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="/bower_resources/bootswatch/lumen/bootstrap.min.css"/>
    <link rel="stylesheet" href="/style/frontend/css/frontend.min.css"/>
    <link rel="stylesheet" href="/bower_resources/font-awesome/css/font-awesome.min.css"/>
</head>
<body>
<div class="se-pre-con"></div>
<nav class="navbar navbar-fixed-top navbar-inverse">
    <div class="col-md-12">
        <div class="navbar-header">
            <a href="#" class="navbar-brand visible-lg visible-md">
                <img src="/style/frontend/img/logo.png" style="width: 20px;display: inline-block;vertical-align: top;" alt=""/>
                <strong>KEMENPU</strong> &middot; Pusat Penelitian dan Pengembangan Pemukiman
            </a>
            <a href="#" class="navbar-brand visible-sm">
                <img src="/style/frontend/img/logo.png" style="width: 20px;display: inline-block;vertical-align: top;" alt=""/>
                Pusat Penelitian &amp; Pengembangan Pemukiman
            </a>
            <a href="#" class="navbar-brand visible-xs">
                <img src="/style/frontend/img/logo.png" style="width: 30px;display: inline-block;vertical-align: middle;" alt=""/>
                <small><strong>KEMENPU</strong> &middot; Puslitbang</small>
            </a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse1">
                <i class="glyphicon glyphicon-search"></i>
            </button>

        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse1">
            <form class="navbar-form pull-right">
                <div class="input-group" style="max-width:470px;">
                    <input type="text" class="form-control input-md" placeholder="Pencarian" name="srch-term" id="srch-term">
                    <div class="input-group-btn">
                        <button class="btn btn-default btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</nav>
<div class="navbar navbar-default" id="subnav">
    <div class="col-md-12">
        <div class="navbar-header">

            <a href="#" style="margin-left:15px; margin-top: 8px" class="navbar-btn btn btn-default btn-primary dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bars"></i>
                E-Katalog
                <small>
                    <i class="glyphicon glyphicon-chevron-down"></i>
                </small>
            </a>
            <ul class="nav dropdown-menu">
                <li><a href="#"><i class="fa fa-folder"></i>&nbsp; Kategori </a></li>
                <li><a href="#"><i class="fa fa-book"></i>&nbsp; Pustaka </a></li>
                <li><a href="#"><i class="fa fa-bookmark"></i>&nbsp; Bookmark</a></li>
                <li class="nav-divider"></li>
                <li><a href="#">Profil PUSLITBANG</a></li>
                <li><a href="#">Hubungi Kami</a></li>
                <li><a href="#">FAQ</a></li>
            </ul>


            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse2">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="#">Beranda</a></li>
                <li><a href="#loginModal" role="button" data-toggle="modal">Masuk</a></li>
                <li><a href="#loginModal" role="button" data-toggle="modal">Daftar</a></li>
                <li><a href="#aboutModal" role="button" data-toggle="modal">FAQ</a></li>
            </ul>
        </div>
    </div>
</div>

<!--main-->
<div class="container" id="main">

<div class="row">
    <div class="col-md-12 item-heading">
        <h2>
            Akses Air Minum
            <a class="btn btn-primary pull-right" href="">Selengkapnya <i class="fa fa-angle-right"></i></a>
        </h2>
    </div>

    <div class="col-sm-3 col-xs-6">
        <div class="panel panel-default">
            <div class="panel-thumbnail"><img src="//placehold.it/450X300/DD3333/EE3333" class="img-responsive"></div>
            <div class="panel-body">
                <p class="lead">Hacker News</p>
            </div>
        </div>
    </div><!--/col-->

    <div class="col-sm-3 col-xs-6">
        <div class="panel panel-default">
            <div class="panel-thumbnail"><img src="//placehold.it/450X300/DD66DD/EE77EE" class="img-responsive"></div>
            <div class="panel-body">
                <p class="lead">Bootstrap</p>
            </div>
        </div>
    </div><!--/col-->

    <div class="col-sm-3 col-xs-6">
        <div class="panel panel-default">
            <div class="panel-thumbnail"><img src="//placehold.it/450X300/2222DD/2222EE" class="img-responsive"></div>
            <div class="panel-body">
                <p class="lead">Social Media</p>
            </div>
        </div>
    </div><!--/col-->

    <div class="col-sm-3 col-xs-6">
        <div class="panel panel-default">
            <div class="panel-thumbnail"><img src="//placehold.it/450X300/02C4FC/3333EE" class="img-responsive"></div>
            <div class="panel-body">
                <p class="lead">Social Media</p>
            </div>
        </div>
    </div><!--/col-->

    <div class="col-sm-3 col-xs-6">
        <div class="panel panel-default">
            <div class="panel-thumbnail"><img src="//placehold.it/450X300/DD3333/EE3333" class="img-responsive"></div>
            <div class="panel-body">
                <p class="lead">Hacker News</p>
            </div>
        </div>
    </div><!--/col-->

    <div class="col-sm-3 col-xs-6">
        <div class="panel panel-default">
            <div class="panel-thumbnail"><img src="//placehold.it/450X300/DD66DD/EE77EE" class="img-responsive"></div>
            <div class="panel-body">
                <p class="lead">Bootstrap</p>
            </div>
        </div>
    </div><!--/col-->

    <div class="col-sm-3 col-xs-6">
        <div class="panel panel-default">
            <div class="panel-thumbnail"><img src="//placehold.it/450X300/2222DD/2222EE" class="img-responsive"></div>
            <div class="panel-body">
                <p class="lead">Social Media</p>
            </div>
        </div>
    </div><!--/col-->

    <div class="col-sm-3 col-xs-6">
        <div class="panel panel-default">
            <div class="panel-thumbnail"><img src="//placehold.it/450X300/02C4FC/3333EE" class="img-responsive"></div>
            <div class="panel-body">
                <p class="lead">Social Media</p>
            </div>
        </div>
    </div><!--/col-->

</div>

<div class="row">
    <div class="col-md-12 item-heading">
        <h2>
            Profil <small>Pusat Penelitian dan Pengembangan Pemukiman</small>
        </h2>
    </div>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <h3>HTML Ipsum Presents</h3>

                <p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p>

                <h3>Header Level 2</h3>

                <ol>
                    <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
                    <li>Aliquam tincidunt mauris eu risus.</li>
                </ol>

                <blockquote><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p></blockquote>

                <h3>Header Level 3</h3>

                <ul>
                    <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
                    <li>Aliquam tincidunt mauris eu risus.</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 item-heading">
        <h2>
            FAQ <small>Pertanyaan yang sering diajukan</small>
        </h2>
    </div>
    <div class="col-md-12">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Collapsible Group Item #1
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Collapsible Group Item #2
                        </a>
                    </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Collapsible Group Item #3
                        </a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="row">
        <div class="col-md-12 item-heading">
            <h2>
                Kontak <small>Ada pertanyaan? Silahkan hubungi kami.</small>
            </h2>
        </div>
        <div class="col-md-12">
            <div class="well well-sm">
                <form>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">
                                    Name</label>
                                <input type="text" class="form-control" id="name" placeholder="Enter name" required="required" />
                            </div>
                            <div class="form-group">
                                <label for="email">
                                    Email Address</label>
                                <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
                                </span>
                                    <input type="email" class="form-control" id="email" placeholder="Enter email" required="required" /></div>
                            </div>
                            <div class="form-group">
                                <label for="subject">
                                    Subject</label>
                                <select id="subject" name="subject" class="form-control" required="required">
                                    <option value="na" selected="">Choose One:</option>
                                    <option value="service">General Customer Service</option>
                                    <option value="suggestions">Suggestions</option>
                                    <option value="product">Product Support</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">
                                    Message</label>
                                <textarea name="message" id="message" class="form-control" rows="9" cols="25" required="required"
                                          placeholder="Message"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary pull-right" id="btnContactUs">
                                Send Message</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
</div><!--/main-->

<!--login modal-->
<div id="loginModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="text-center">Masuk</h2>
            </div>
            <div class="modal-body">
                <form class="form col-md-12 center-block">
                    <div class="form-group">
                        <input type="text" class="form-control input-lg" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control input-lg" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary btn-lg btn-block">Sign In</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="col-md-12">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>


<!--about modal-->
<div id="aboutModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="text-center">About</h2>
            </div>
            <div class="modal-body">
                <div class="col-md-12 text-center">
                    <a href="http://www.bootply.com/DwnjTNuvVt">This Bootstrap Template</a><br>was made with <i class="glyphicon glyphicon-heart"></i> by <a href="http://bootply.com/templates">Bootply</a>
                    <br><br>
                    <a href="https://github.com/iatek/bootstrap-google-plus">GitHub Fork</a>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">OK</button>
            </div>
        </div>
    </div>
</div>
<script src="/bower_resources/jquery/dist/jquery.min.js"></script>
<script src="/bower_resources/bootstrap/dist/js/bootstrap.min.js"></script>
<script>
    /* Preloader */
    //paste this code under head tag or in a seperate js file.
    // Wait for window load
    $(window).load(function() {
        // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");;
    });

    /* jQuery toggle layout */
    $('#btnToggle').click(function(){
        if ($(this).hasClass('on')) {
            $('#main .col-md-6').addClass('col-md-4').removeClass('col-md-6');
            $(this).removeClass('on');
        }
        else {
            $('#main .col-md-4').addClass('col-md-6').removeClass('col-md-4');
            $(this).addClass('on');
        }
    });
</script>
</body>
</html>