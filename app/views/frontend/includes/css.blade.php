{{--Bootstrap--}}
<link rel="stylesheet" href="{{ url('/bower_resources/bootswatch/lumen/bootstrap.min.css') }}"/>
{{--Frontend Style--}}
<link rel="stylesheet" href="{{ url('/style/frontend/css/frontend.min.css') }}"/>
{{--Font Awesome--}}
<link rel="stylesheet" href="{{ url('/bower_resources/font-awesome/css/font-awesome.min.css') }}"/>
{{-- Hover.css --}}
<link rel="stylesheet" href="{{ url('/bower_resources/Hover/css/hover-min.css') }}" />