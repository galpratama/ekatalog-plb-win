<nav class="navbar navbar-fixed-top navbar-inverse">
    <div class="col-md-12">
        <div class="navbar-header">
            <a href="{{ url('/') }}" class="navbar-brand visible-lg visible-md">
                <img src="{{ url('/style/frontend/img/logo.png') }}" style="width: 20px;display: inline-block;vertical-align: top;" alt=""/>
                <strong>Kementerian PU</strong> &middot; Pusat Penelitian dan Pengembangan Pemukiman
            </a>
            <a href="{{ url('/') }}" class="navbar-brand visible-sm">
                <img src="{{ url('/style/frontend/img/logo.png') }}" style="width: 20px;display: inline-block;vertical-align: top;" alt=""/>
                Pusat Penelitian &amp; Pengembangan Pemukiman
            </a>
            <a href="{{ url('/') }}" class="navbar-brand visible-xs">
                <img src="{{ url('/style/frontend/img/logo.png') }}" style="width: 30px;display: inline-block;vertical-align: middle;" alt=""/>
                <small><strong>Kementerian PU</strong> &middot; Puslitbang</small>
            </a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse1">
                <i class="glyphicon glyphicon-search"></i>
            </button>

        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse1">
            <form class="navbar-form pull-right">
                <div class="input-group" style="max-width:470px;">
                    <input type="text" class="form-control input-md" placeholder="Pencarian" name="srch-term" id="srch-term">
                    <div class="input-group-btn">
                        <button class="btn btn-default btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</nav>
<div class="navbar navbar-default" id="subnav">
    <div class="col-md-12">
        <div class="navbar-header">
            <a style="margin-left:15px; margin-top: 8px" class="navbar-btn btn btn-default btn-primary" href="{{ url('/') }}"><i class="fa fa-home"></i></a>
            <a href="#" class="navbar-btn btn btn-default btn-primary dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bars"></i>
                E-Katalog
                <small>
                    <i class="glyphicon glyphicon-chevron-down"></i>
                </small>
            </a>
            <?php $setting = Setting::find(1); ?>
            <a href="{{ $setting->playstore_link }}" target="_blank" class="hidden-xs">
                <img src="{{ url('/style/frontend/img/playstore.png') }}" alt="Unduh Aplikasi di Play Store" style="height: 38px;"/>
            </a>
            <ul class="nav dropdown-menu">
                <li><a href="{{ url('/category') }}"><i class="fa fa-folder"></i>&nbsp; Kategori </a></li>
                @if(Auth::check())
                    <li><a href="{{ url('/favorite') }}"><i class="fa fa-book"></i>&nbsp; Pustaka </a></li>
                @endif
                <li class="nav-divider"></li>
                <?php $page = Page::all(); ?>
                @foreach($page as $data)
                    <li><a href="{{ url('page') . '/' . $data->id }}">{{ $data->title }}</a></li>
                @endforeach
                <li><a href="{{ url('/contact') }}">Hubungi Kami</a></li>
                <li><a href="{{ url('/faq') }}">FAQ</a></li>
            </ul>


            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse2">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="/">Beranda</a></li>
                <li><a href="{{ url('/') }}/faq">FAQ</a></li>
                @if(Auth::check())
                <li>
                    <a href="{{ url('/') }}/logout" role="button" data-toggle="dropdown">
                    <i class="fa fa-user"></i>
                    {{ Auth::user()->full_name }} <i class="fa fa-caret-down"></i></a>
                    <ul class="nav dropdown-menu">
                        @if(Auth::user()->role == 'admin')
                        <li><a href="{{ url('/') }}/backend"><strong>Panel Admin</strong></a></li>
                        <li class="nav-divider"></li>
                        @endif
                        <li><a href="#editProfile" role="button" data-toggle="modal">Edit Profil </a></li>
                        <li><a href="{{ url('/') }}/logout">Keluar</a></li>
                    </ul>
                </li>
                @else
                <li><a href="#loginModal" role="button" data-toggle="modal">Masuk</a></li>
                <li><a href="#registerModal" role="button" data-toggle="modal">Daftar</a></li>
                @endif

            </ul>
        </div>
    </div>
</div>