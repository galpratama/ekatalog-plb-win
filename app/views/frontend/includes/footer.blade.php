<footer class="footer hidden-xs">
    <div class="container">
        <div class="pull-left">
            <p>COPYRIGHT &middot; 2015 PUSLITBANG PERMUKIMAN KEMENTERIAN PEKERJAAN UMUM</p>
        </div>
        <div class="pull-right">
            <ul>
                <?php $link = Link::all(); ?>
                @foreach($link as $data)
                    <li><a href="{{ $data->url }}">{{ $data->title }}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
</footer>

@if(Auth::check())

    <!--Edit profile modal-->
    <div id="editProfile" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="text-center">Ubah Profil</h2>
                </div>
                <div class="modal-body">
                    {{ Form::open(array('url' => 'profile', 'class' => 'form col-md-12 center-block')) }}
                    <div class="form-group">
                        <input type="text" name="name" class="form-control input-lg" placeholder="Nama Pengguna" value="{{ Auth::user()->name }}">
                        <div class="input-group-addon">Nama Pengguna</div>
                    </div>
                    <div class="form-group">
                        <input type="text" name="full_name" class="form-control input-lg" placeholder="Nama Lengkap" value="{{ Auth::user()->full_name }}">
                        <div class="input-group-addon">Nama Lengkap</div>
                    </div>
                    <div class="form-group">
                        <input type="text" name="email" class="form-control input-lg" placeholder="Surel" value="{{ Auth::user()->email }}">
                        <div class="input-group-addon">Surel</div>
                    </div>
                    <div class="form-group">
                        <input type="text" name="phone" class="form-control input-lg" placeholder="No. Telepon" value="{{ Auth::user()->phone }}">
                        <div class="input-group-addon">No. Telepon</div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Perbarui</button>
                    </div>
                    {{ Form::close() }}
                </div>
                <div class="modal-footer">
                    <div class="col-md-12">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Batalkan</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endif

<!--login modal-->
<div id="loginModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="text-center">Masuk</h2>
            </div>
            <div class="modal-body">
                {{ Form::open(array('url' => 'login', 'class' => 'form col-md-12 center-block')) }}
                    <div class="form-group">
                        <input type="text" name="name" class="form-control input-lg" placeholder="Nama Pengguna">
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control input-lg" placeholder="Kata Sandi">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Masuk</button>
                    </div>
                {{ Form::close() }}
            </div>
            <div class="modal-footer">
                <div class="col-md-12">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Batalkan</button>
                </div>
            </div>
        </div>
    </div>
</div>


<!--register modal-->
<div id="registerModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="text-center">Daftar</h2>
            </div>
            <div class="modal-body">
                {{ Form::open(array('url' => 'register', 'class' => 'form col-md-12 center-block')) }}
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <input type="text" name="full_name" id="full_name" class="form-control input-lg" placeholder="Nama Lengkap" tabindex="2">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" name="name" id="name" class="form-control input-lg" placeholder="Nama Pengguna" tabindex="3">
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" id="email" class="form-control input-lg" placeholder="Alamat Surel" tabindex="4">
                    </div>
                    <div class="form-group">
                        <input type="text" name="phone" id="phone" class="form-control input-lg" placeholder="Nomor Telepon" tabindex="4">
                    </div>
                    <div class="form-group">
                        <label for="file">Foto Profil</label>
                        <input type="file" name="file" id="file" class="form-control input-lg" placeholder="Unggah Foto" tabindex="4">
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="password" name="password" id="password" class="form-control input-lg" placeholder="Sandi" tabindex="5">
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-lg" placeholder="Ulang Sandi" tabindex="6">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12"><input type="submit" value="Daftar" class="btn btn-primary btn-block btn-lg" tabindex="7"></div>
                    </div>
                {{ Form::close() }}
            </div>
            <div class="modal-footer">
                <div class="col-md-12"><br/>
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Batalkan</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!--about modal-->
<div id="aboutModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="text-center">About</h2>
            </div>
            <div class="modal-body">
                <div class="col-md-12 text-center">
                    <a href="http://www.bootply.com/DwnjTNuvVt">This Bootstrap Template</a><br>was made with <i class="glyphicon glyphicon-heart"></i> by <a href="http://bootply.com/templates">Bootply</a>
                    <br><br>
                    <a href="https://github.com/iatek/bootstrap-google-plus">GitHub Fork</a>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">OK</button>
            </div>
        </div>
    </div>
</div>
<script src="{{ url('/bower_resources/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ url('/bower_resources/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script>
    /* Preloader */
    //paste this code under head tag or in a seperate js file.
    // Wait for window load
    $(window).load(function() {
        // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");
        $('.row-item').fadeIn();
    });

    function calcHeight()
    {
        //find the height of the internal page
        var the_height=
            document.getElementById('the_iframe').contentWindow.
                document.body.scrollHeight;

        //change the height of the iframe
        document.getElementById('the_iframe').height=
            the_height;
    }


    /* jQuery toggle layout */
    $('#btnToggle').click(function(){
        if ($(this).hasClass('on')) {
            $('#main .col-md-6').addClass('col-md-4').removeClass('col-md-6');
            $(this).removeClass('on');
        }
        else {
            $('#main .col-md-4').addClass('col-md-6').removeClass('col-md-4');
            $(this).addClass('on');
        }
    });
</script>

<script src="{{ url('/bower_resources/masonry/dist/masonry.pkgd.min.js') }}"></script>
<script src="{{ url('/bower_resources/imagesloaded/imagesloaded.js') }}"></script>
<script>
  imagesLoaded( document.querySelector('.row-item'), function( instance ) {
    var $container = $('.row-item');
      // initialize
      $container.masonry({
        itemSelector: '.panel-item'
      });
    $('.panel-item').fadeIn();
  });
</script>