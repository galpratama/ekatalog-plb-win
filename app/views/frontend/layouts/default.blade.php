<!doctype html>
<html lang="en">
<head>
    <title>
        @yield('title','Home') | Ekatalog Puslitbang
    </title>
    @include('frontend.includes.top')
    @include('frontend.includes.css')

</head>
<body>
    <div class="se-pre-con"></div>
    @include('frontend.includes.header')

    <div class="container" id="main">
        @yield('content', 'This is empty page')
    </div>

    {{-- Load Footer and required javascript --}}
    @include('frontend.includes.footer')

    {{--load additional javascript --}}
    @yield('script')

    </body>
</html>