@extends('frontend.layouts.default')

@section('title')
    {{ $page->title }}
@endsection

@section('content')


    <div class="container" id="main">
        <div class="row">
            <div class="col-md-12 item-heading">
                <h2>
                   {{ $page->title }}
                </h2>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        {{ $page->description }}
                    </div>
                </div>
            </div>

        </div>
    </div>

@stop