@extends('frontend.layouts.default')

@section('content')
<div class="container" id="main">
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">
            <i class="fa fa-info"></i> {{ Session::get('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="row row-item">
        {{--<div class="col-md-12 item-heading">--}}
            {{--<h2>--}}
                {{--Akses Air Minum--}}
                {{--<a class="btn btn-primary pull-right" href="">Selengkapnya <i class="fa fa-angle-right"></i></a>--}}
            {{--</h2>--}}
        {{--</div>--}}
        @foreach($item as $key => $data)
             <div class="col-sm-3 col-xs-6 panel-item">
                <a href="{{ url('/item') . '/' . $data->id }}">
                <div class="panel panel-default hvr-grow-shadow">
                    <div class="panel-thumbnail"><img src="{{ url('/uploads/item') .'/' .  $data->picture }}" class="img-responsive"></div>
                    <div class="panel-body">
                        <h3 class="item-title">
                            {{{ $data->title }}}
                            <i class="fa fa-book pull-right"></i>
                        </h3>
                    </div>
                </div>
                </a>
            </div>
        @endforeach
    </div>

    <div class="row text-center">
        {{ $item->links(); }}
    </div>

</div>

@stop