@extends('frontend.layouts.default')

@section('title')
FAQ
@endsection

@section('content')
<div class="container" id="main">
    <div class="row">

        <div class="col-md-12 item-heading">
            <h2>
                FAQ <small>Pertanyaan yang sering diajukan</small>
            </h2>
        </div>
        <div class="col-md-12">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <?php $increment = 1 ?>
                 @foreach($faq as $key => $value)
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $increment }}" aria-expanded="true" aria-controls="collapseOne">
                                {{ $value->title }}
                            </a>
                        </h4>
                    </div>
                    <div id="collapse{{ $increment }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            {{ $value->description }}
                        </div>
                    </div>
                </div>
                <?php $increment++ ?>
                 @endforeach
            </div>
        </div>
    </div>

</div>

@stop