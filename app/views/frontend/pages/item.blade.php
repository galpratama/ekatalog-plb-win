@extends('frontend.layouts.default')

@section('title')
Katalog "{{ $item->title }}"
@endsection

@section('content')

@foreach($category as $category) @endforeach

<div class="container" id="main">
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">
            <i class="fa fa-info"></i> {{ Session::get('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12 item-heading">
            <h2>
                <a href="javascript:history.go(-1)" class="btn btn-primary"><i class="fa fa-arrow-left"></i></a> {{ $item->title }}
                <a class="btn btn-warning btn-sm pull-right" href="{{ url('/category') . '/' .  $category->id }}"><i class="fa fa-folder"></i> {{ $category->title }}</a>
            </h2>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <!-- 16:9 aspect ratio -->
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="{{ url('/packages/pdfjs/web/viewer.html?file=') . url('/uploads/docs') . '/' . $item->document }}"></iframe>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            <!--
            function popup(url)
            {
                params  = 'width='+screen.width;
                params += ', height='+screen.height;
                params += ', top=0, left=0'
                params += ', fullscreen=yes';
    
                newwin=window.open(url,'windowname4', params);
                if (window.focus) {newwin.focus()}
                return false;
            }
            // -->
        </script>
        <div class="col-md-4">
            <h4>
                Deskripsi Katalog
            </h4>

            <p>
            {{ $item->description }}
            </p>

            @if($item->downloadable === 1)
                <a href="{{ url('/uploads/docs') . '/' . $item->document }}" class="btn btn-primary btn-lg" target="_blank"><i class="fa fa-download"></i></a>
            @endif

            @if(Auth::check())
                @if($favoriteNotExists)
                    <a class="btn btn-danger btn-lg" href="{{ url('/favorite') . '/' .  $item->id }}"><i class="fa fa-book"></i> Simpan ke Pustaka</a>
                @endif
            @endif

            <a onclick="popup('{{ url('/packages/pdfjs/web/viewer.html?file=') . url('/uploads/docs') . '/' . $item->document }}')"
               href="#"
               class="btn btn-success btn-lg" href="">
                <i class="fa fa-arrows-alt"></i> Perbesar
            </a>
        </div>
    </div>
</div>

@stop