@extends('frontend.layouts.default')

@section('title')
    Hubungi Kami
@endsection

@section('content')
    <div class="container" id="main">
        <!-- will be used to show any messages -->
        @if (Session::has('message'))
            <div class="alert alert-info">
                <i class="fa fa-info"></i> {{ Session::get('message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="row">

            <div class="col-md-12 item-heading">
                <h2>
                    Hubungi Kami <small>Silahkan ajukan pertanyaan anda disini</small>
                </h2>
            </div>
            <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="well well-sm">
                                {{ Form::open(array('url' => 'contact', 'method' => 'POST')) }}
                                <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name">Nama</label>
                                                <input type="text" class="form-control" id="name" name="name" placeholder="Masukkan Nama" required="required" />
                                            </div>
                                            <div class="form-group">
                                                <label for="email"> Surel</label>
                                                <div class="input-group">
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
                                                 <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" required="required" /></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="message">Pesan</label>
                                                <textarea name="message" id="message" class="form-control" rows="9" cols="25" required="required" placeholder="Pesan Anda"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-primary pull-right" id="btnContactUs">
                                                Kirim Pesan</button>
                                        </div>
                                    </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <form>
                                <legend><span class="fa fa-map-marker"></span> Kantor Kami</legend>
                                <?php $setting = Setting::find(1); ?>
                                {{ $setting->address }}
                            </form>
                        </div>
                    </div>
            </div>
        </div>

    </div>

@stop