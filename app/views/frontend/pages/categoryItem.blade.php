@extends('frontend.layouts.default')

@section('content')
<div class="container" id="main">
    <div class="row">
        <div class="col-md-12 item-heading">
            <h2>
                <a href="{{ url('/category') }}" class="btn btn-primary"><i class="fa fa-arrow-left"></i>
                    Kategori </a> {{ $category->title }}
                {{--<a class="btn btn-primary pull-right" href="">Selengkapnya <i class="fa fa-angle-right"></i></a>--}}
            </h2>
        </div>
    </div>
    <div class="row row-item">

        @foreach($item as $key => $data)
             <div class="col-sm-3 col-xs-6 panel-item">
                <a href="{{ url('/item') . '/' .  $data->id }}">
                <div class="panel panel-default hvr-grow-shadow">
                    <div class="panel-thumbnail"><img src="{{ url('/uploads/item') .'/' .  $data->picture }}" class="img-responsive"></div>
                    <div class="panel-body">
                        <h3 class="item-title">
                            {{{ $data->title }}}
                            <i class="fa fa-book pull-right"></i>
                        </h3>
                    </div>
                </div>
                </a>
            </div>
        @endforeach
    </div>

    <div class="row text-center">
        {{ $item->links(); }}
    </div>

</div>

@stop