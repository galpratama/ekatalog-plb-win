<!doctype html>
<html>
    <head>
        @include('backend.includes.top')
        @include('backend.includes.css')
    </head>
    <body class="skin-blue">
        <header class="header">
            @include('backend.includes.header')
        </header>

        <div class="wrapper row-offcanvas row-offcanvas-left">

            <aside class="left-side sidebar-offcanvas">
                <section class="sidebar">
                    @include('backend.includes.sidebar')
                </section>
            </aside>


            <aside class="right-side">
                <section class="content-header">
                    <h1>
                        @yield('heading', 'Administrator')
                    </h1>
                    <ol class="breadcrumb">
                        @yield('breadcumb')
                    </ol>
                </section>
                <section class="content">
                    @yield('content', 'This is empty page')
                </section>
            </aside>

        </div>

        {{-- load required javascript --}}
        @include('backend.includes.footer')

        {{-- load additional javascript --}}
        @yield('script')

    </body>
</html>