<!doctype html>
<html>
    <head>
        <title>Look at me Login</title>
        <link rel="stylesheet" href="{{ url('/') }}/bower_resources/bootstrap/dist/css/bootstrap.min.css" />
        <link rel="stylesheet" href="{{ url('/') }}/bower_resources/font-awesome/css/font-awesome.min.css" />
        <link rel="stylesheet" href="{{ url('/') }}/style/backend/css/login.min.css" />
    </head>
    <body>

    <div class="container" style="margin-top:10%">
      <div class="col-md-3 col-md-offset-4">
        <div class="panel member_signin">
          <div class="panel-body">
            <div class="fa_user">
              <i class="fa fa-user"></i>
            </div>
            <p class="member">Masuk Administrator</p>
            {{ Form::open(array('url' => 'back-login')) }}
            <p>
                {{ $errors->first('name') }}
                {{ $errors->first('password') }}
            </p>
              <div class="form-group">
                <label for="exampleInputEmail1" class="sr-only">Nama Pengguna</label>
                <div class="input-group">
                  <input type="text" name="name" class="form-control" id="exampleInputEmail1"
                    placeholder="Nama Pengguna">
                </div>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1" class="sr-only">Kata Sandi</label>
                <div class="input-group">
                  <input type="password" name="password" class="form-control" id="exampleInputPassword1"
                    placeholder="Kata Sandi">
                </div>
              </div>
              <button type="submit" class="btn btn-primary btn-md login">Masuk</button>
                {{ Form::close() }}

          </div>
        </div>
      </div>
    </div>
  </body>
</html>
