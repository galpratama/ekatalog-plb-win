@extends('backend.layouts.default')

@section('content')

    <div class="row">
        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>
                        <?php $number = Item::count();?>
                        {{ $number }}
                    </h3>
                    <p>
                        Konten Katalog
                    </p>
                </div>
                <div class="icon">
                    <i class="fa fa-th"></i>
                </div>
                <a href="{{ url('/backend/category') }}" class="small-box-footer">
                    Buka <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>
                        <?php $number = Category::count();?>
                        {{ $number }}
                    </h3>
                    <p>
                        Kategori Katalog
                    </p>
                </div>
                <div class="icon">
                    <i class="fa fa-list"></i>
                </div>
                <a href="{{ url('/backend/category') }}" class="small-box-footer">
                    Buka <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>
                        <?php $number = User::count();?>
                        {{ $number }}
                    </h3>
                    <p>
                        Pengguna
                    </p>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
                <a href="{{ url('/backend/user') }}" class="small-box-footer">
                    Buka <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>
                        <?php $number = Page::count();?>
                        {{ $number }}
                    </h3>
                    <p>
                        Halaman
                    </p>
                </div>
                <div class="icon">
                    <i class="fa fa-file"></i>
                </div>
                <a href="{{ url('/backend/page') }}" class="small-box-footer">
                    Buka <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>
                        <?php $number = Link::count();?>
                        {{ $number }}
                    </h3>
                    <p>
                        Tautan
                    </p>
                </div>
                <div class="icon">
                    <i class="fa fa-chain"></i>
                </div>
                <a href="{{ url('/backend/link') }}" class="small-box-footer">
                    Buka <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>
                        <?php $number = Contact::count();?>
                        {{ $number }}
                    </h3>
                    <p>
                        Pesan
                    </p>
                </div>
                <div class="icon">
                    <i class="fa fa-envelope-o"></i>
                </div>
                <a href="{{ url('/backend/contact') }}" class="small-box-footer">
                    Buka <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
    </div>

@stop