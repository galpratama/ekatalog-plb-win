@extends('backend.layouts.default')

@section('heading')
Kategori Katalog
@endsection

@section('breadcumb')

<li>
    <a href="{{ url('/') }}/backend">Home</a>
</li>
<li class="active">
    <a href="{{ url('/') }}/backend/category">Katalog</a>
</li>

@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Daftar Kategori Katalog</h3>
                <div class="box-tools pull-right">
                <button type="button"  class="btn btn-md btn-primary" data-toggle="modal" data-target="#addKategori">
                    <i class="fa fa-plus-square"></i> Tambah Kategori
                </button>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
            <!-- will be used to show any messages -->
            @if (Session::has('message'))
                <div class="alert alert-info">
                    <i class="fa fa-info"></i> {{ Session::get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
                <table class="table table-bordered table-striped datatable">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Judul</th>
                            <th>Deskripsi</th>
                            <th width="18%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                         <?php $increment = 1 ?>
                         @foreach($category as $key => $value)
                             <tr>
                                <td>{{ $increment }}</td>
                                <td>
                                    <strong>{{{ strip_tags($value->title) }}}</strong>
                                </td>
                                <td>{{{ substr(strip_tags($value->description), 0, 100) }}} [..]</td>
                                <td>
                                    <a href="{{ url('/') }}/backend/item/{{{ strip_tags($value->id) }}}"
                                        class="btn btn-sm btn-success btn-md">
                                    <i class="fa fa-eye"></i> Lihat Katalog
                                    </a>
                                    <button type="button"
                                            class="btn btn-sm btn-info btn-md"
                                            data-toggle="modal"
                                            data-target="#editKategori{{ $value->id }}">
                                      <i class="fa fa-pencil"></i>
                                    </button>
                                    <button type="button"
                                            class="btn btn-sm btn-danger btn-md"
                                            data-toggle="modal"
                                            data-target="#deleteKategori{{ $value->id }}">
                                      <i class="fa fa-trash-o"></i>
                                    </button>
                                </td>
                             </tr>
                         <?php $increment++ ?>
                         @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No.</th>
                            <th>Judul</th>
                            <th>Deskripsi</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

{{--Modal--}}

@foreach($category as $key => $value)
<!-- Edit Modal -->
<div class="modal modal-wide fade" id="editKategori{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="editKategoriLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Sunting <strong>"{{{ strip_tags($value->title) }}}"</strong></h4>
      </div>

      {{ Form::open(array('route' => array('backend.category.update', $value->id), 'method' => 'PUT', 'files' => true)) }}

      <div class="modal-body">
            <label for="title">Judul:</label>
            <input type="text"
                    name="title"
                    value="{{{ strip_tags($value->title) }}}"
                    class="form-control input-lg"
                    placeholder="Contoh: Pembangunan Kota"
                    required/>

            <textarea name="description" class="tinymce" cols="30" rows="10">{{ $value->description }}</textarea>
            <label for="file">Ganti Foto Kategori Katalog (Opsional)</label>
            <input type="file"
                 name="file"
                 class="form-control input-lg"
                 accept="image/*"/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Batalkan</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan Perubahan</button>
      </div>

      {{ Form::close() }}

    </div>
  </div>
</div>
@endforeach

@foreach($category as $key => $delete)
<!-- Delete Modal -->
<div class="modal fade" id="deleteKategori{{ $delete->id }}" tabindex="-1" role="dialog" aria-labelledby="editKategoriLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Hapus <strong>"{{{ strip_tags($delete->title) }}}"</strong></h4>
      </div>
      <div class="modal-body">

        <p>Anda yakin ingin menghapus <strong>"{{{ strip_tags($delete->title) }}}"</strong> beserta isinya?</p>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Batalkan</button>
        {{ Form::open(array('route' => array('backend.category.destroy', $delete->id), 'method' => 'DELETE')) }}
            <button type="submit" class="btn btn-primary"><i class="fa fa-trash"></i> Hapus</button>
        {{ Form::close() }}
      </div>
    </div>
  </div>
</div>
@endforeach


{{--Add Kategori Modal--}}
<div class="modal modal-wide fade" id="addKategori" tabindex="-1" role="dialog" aria-labelledby="addKategoriLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Kategori Katalog</h4>
      </div>

       {{ Form::open(array('method' => 'POST','files' => true)) }}

      <div class="modal-body">
            <label for="title">Judul:</label>
            <input type="text"
                    name="title"
                    class="form-control input-lg"
                    placeholder="Contoh: Pembangunan Kota"
                    required/>
            <textarea name="description" class="tinymce" cols="30" rows="10"></textarea>
            <label for="file">Gambar:</label>
            <input type="file"
                    name="file"
                    class="form-control input-lg"
                    accept="image/*"
                    required/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Batalkan</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan Perubahan</button>
      </div>

      {{ Form::close() }}

    </div>
  </div>
</div>

@endsection
