@extends('backend.layouts.default')

@section('heading')
Administrator
@endsection

@section('breadcumb')

<li>
    <a href="{{ url('/') }}/backend">Beranda</a>
</li>
<li class="active">
    <a href="{{ url('/') }}/backend/user">Administrator</a>
</li>

@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Daftar Administrator</h3>
                <div class="box-tools pull-right">
                <button type="button"  class="btn btn-md btn-primary" data-toggle="modal" data-target="#addUser">
                    <i class="fa fa-plus-square"></i> Tambah Administrator
                </button>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
                <!-- will be used to show any messages -->
                @if (Session::has('message'))
                    <div class="alert alert-info">
                        <i class="fa fa-info"></i> {{ Session::get('message') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <table class="table table-bordered table-striped datatable">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Lengkap</th>
                            <th>Nama Pengguna</th>
                            <th>Surel</th>
                            <th>Tanggal Terdaftar</th>
                            <th width="4%">Status</th>
                            <th width="15%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                         <?php $increment = 1 ?>
                         @foreach($user as $key => $value)
                             <tr>
                                <td>{{ $increment }}</td>
                                <td>
                                    <strong>{{{ $value->full_name }}}</strong>
                                </td>
                                <td>{{{ $value->name }}}</td>
                                <td>{{{ $value->email }}}</td>
                                <td>{{ date('d M Y \(H:i\)', strtotime($value->created_at)) }}</td>
                                <td>

                                    {{--If condition for user status--}}
                                    <?php $status = ($value->status == 1 ? 'btn-success' : 'btn-danger') ?>
                                    <?php $status_icon = ($value->status == 1 ? 'fa-check-circle' : 'fa-times-circle') ?>

                                    <button type="button"
                                            class="btn btn-sm {{ $status }}"
                                            data-toggle="modal"
                                            data-target="#changeUserStatus{{ $value->id }}">
                                      <i class="fa {{ $status_icon }}"></i>
                                    </button>
                                </td>
                                <td>
                                    <button type="button"
                                            class="btn btn-sm btn-info btn-md"
                                            data-toggle="modal"
                                            data-target="#editUser{{ $value->id }}">
                                      <i class="fa fa-pencil"></i>
                                    </button>
                                    <button type="button"
                                            class="btn btn-sm btn-danger btn-md"
                                            data-toggle="modal"
                                            data-target="#changeUserPicture{{ $value->id }}">
                                      <i class="fa fa-picture-o"></i>
                                    </button>
                                     <button type="button"
                                            class="btn btn-sm btn-warning btn-md"
                                            data-toggle="modal"
                                            data-target="#changeUserPassword{{ $value->id }}">
                                      <i class="fa fa-key"></i>
                                    </button>
                                </td>
                             </tr>
                         <?php $increment++ ?>
                         @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No.</th>
                            <th>Nama Lengkap</th>
                            <th>Nama Pengguna</th>
                            <th>Surel</th>
                            <th>Tanggal Terdaftar</th>
                            <th width="4%">Status</th>
                            <th width="15%">Aksi</th>
                        </tr>
                    </tfoot>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

{{--Modal--}}

@foreach($user as $key => $value)
<!-- Edit Modal -->
<div class="modal modal-wide fade" id="editUser{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="editUserLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Sunting Administrator <strong>"{{ $value->full_name }}"</strong></h4>
      </div>

      {{ Form::open(array('route' => array('backend.administrator.update', $value->id), 'method' => 'PUT')) }}

      <div class="modal-body">
            <label for="full_name">Nama Lengkap:</label>
                        <input type="text"
                                name="full_name"
                                class="form-control input-lg"
                                placeholder="Contoh: John Doe"
                                value="{{ $value->full_name }}"
                                required/>
                        <label for="name">Nama Pengguna (Username):</label>
                        <input type="text"
                                name="name"
                                class="form-control input-lg"
                                placeholder="Contoh: johndoe"
                                value="{{ $value->name }}"
                                required/>
                        <label for="email">Surel (Alamat Email):</label>
                        <input type="text"
                                name="email"
                                class="form-control input-lg"
                                placeholder="Contoh: johndoe@gmail.com"
                                value="{{ $value->email }}"
                                required/>
                        <label for="phone">Nomor Telepon</label>
                        <input type="text"
                                name="phone"
                                class="form-control input-lg"
                                placeholder="Contoh: 085712345678"
                                value="{{ $value->phone }}"
                                required/>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Batalkan</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan Perubahan</button>
      </div>

      {{ Form::close() }}

    </div>
  </div>
</div>

<!-- Change Picture Modal -->
<div class="modal fade" id="changeUserPicture{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="editUserLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Ganti Foto Profil <strong>"{{ $value->full_name }}"</strong></h4>
      </div>
      <div class="modal-body">

        {{ Form::open(array('route' => array('backend.administrator.update', $value->id), 'method' => 'PUT')) }}
        <i>Work in Progress...</i>
        {{ Form::close() }}

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Batalkan</button>
        <button type="button" class="btn btn-primary"><i class="fa fa-save"></i> Ganti Foto Profil</button>
      </div>
    </div>
  </div>
</div>

<!-- Change Password Modal -->
<div class="modal fade" id="changeUserPassword{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="editUserLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Ganti Kata Sandi <strong>"{{ $value->full_name }}"</strong></h4>
      </div>
      <div class="modal-body">

        {{ Form::open(array('route' => array('backend.administrator.update', $value->id), 'method' => 'PUT')) }}
        <i>Work in Progress...</i>
        {{ Form::close() }}

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Batalkan</button>
        <button type="button" class="btn btn-primary"><i class="fa fa-save"></i> Ganti Sandi</button>
      </div>
    </div>
  </div>
</div>


<!-- Change Status Modal -->
<div class="modal fade" id="changeUserStatus{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="editUserLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Ganti Status Administrator <strong>"{{ $value->full_name }}"</strong></h4>
      </div>
      <div class="modal-body">

        {{ Form::open(array('route' => array('backend.administrator.update', $value->id), 'method' => 'PUT')) }}
        <i>Work in Progress...</i>
        {{ Form::close() }}

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Batalkan</button>
        <button type="button" class="btn btn-primary"><i class="fa fa-save"></i> Ganti</button>
      </div>
    </div>
  </div>
</div>

@endforeach

{{--Add User Modal--}}

<div class="modal modal-wide fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="addUserLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Administrator</h4>
      </div>

      {{ Form::open(array('method' => 'POST', 'files' => true)) }}

      <div class="modal-body">

            <label for="full_name">Nama Lengkap:</label>
            <input type="text"
                    name="full_name"
                    class="form-control input-lg"
                    placeholder="Contoh: John Doe"
                    required/>
            <label for="name">Nama Pengguna (Username):</label>
            <input type="text"
                    name="name"
                    class="form-control input-lg"
                    placeholder="Contoh: johndoe"
                    required/>
            <label for="password">Kata Sandi:</label>
            <input type="password"
                    name="password"
                    class="form-control input-lg"
                    placeholder="***********"
                    required/>
            <label for="email">Surel (Alamat Email):</label>
            <input type="text"
                    name="email"
                    class="form-control input-lg"
                    placeholder="Contoh: johndoe@gmail.com"
                    required/>
            <label for="phone">Nomor Telepon</label>
            <input type="text"
                    name="phone"
                    class="form-control input-lg"
                    placeholder="Contoh: 085712345678"
                    required/>

            <label for="file">Foto profil</label>
            <input type="file"
                    name="file"
                    class="form-control input-lg"
                    placeholder="Contoh: 085712345678"
                    accept="image/*"
                    required/>

            {{--<label for="picture_b64">Foto Profil</label>--}}
            {{--<input type="file"--}}
                    {{--id="img_b64"--}}
                    {{--name="picture_b64"--}}
                    {{--class="form-control input-lg"/>--}}
            {{--<label>Pratinjau:</label>--}}
            {{--<br/>--}}
            {{--<img id="img" width="25%" class="img-thumbnail" src="" />--}}

            {{--<textarea name="picture" id="base" hidden></textarea>--}}

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Batalkan</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan Perubahan</button>
      </div>

      {{ Form::close() }}

    </div>
  </div>
</div>

@endsection

@section('script')

<script>
function readImage(input) {
    if ( input.files && input.files[0] ) {
        var FR= new FileReader();
        FR.onload = function(e) {
             $('#img').attr( "src", e.target.result );
             $('#base').text( e.target.result );
        };
        FR.readAsDataURL( input.files[0] );
    }
}

$("#img_b64").change(function(){
    readImage(this);
});
</script>
@endsection
