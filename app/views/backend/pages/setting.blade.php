@extends('backend.layouts.default')

@section('heading')
    Pengaturan Situs
@endsection

@section('breadcumb')

    <li>
        <a href="{{ url('/') }}/backend">Home</a>
    </li>
    <li class="active">
        <a href="{{ url('/') }}/backend/setting">Pengaturan Situs</a>
    </li>

@endsection

@section('content')

    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Pengaturan Situs</h3>
                    <div class="box-tools pull-right">

                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <!-- will be used to show any messages -->
                    @if (Session::has('message'))
                        <div class="alert alert-info">
                            <i class="fa fa-info"></i> {{ Session::get('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    {{ Form::open(array('url' => '/backend/setting/1', 'method' => 'PUT')) }}
                        <div class="form-group">
                            <label>Tautan Aplikasi ke Play Store</label>
                            <input required value="{{ $setting->playstore_link }}" type="text" name="playstore_link" class="form-control" placeholder="Contoh: https://play.google.com/store/apps/details?id=com.aplikasi.puslitbang">
                        </div>

                    <div class="form-group">
                        <label>Alamat pada Halaman Kontak</label>
                        <textarea name="address" class="tinymce" cols="30" rows="10">{{ $setting->address }}</textarea>
                    </div>

                        <div class="form-group">
                            <button type="submit" class="form-control btn btn-primary">Simpan perubahan</button>
                        </div>
                    {{ Form::close() }}


                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>


@endsection