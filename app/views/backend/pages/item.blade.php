@extends('backend.layouts.default')

@section('heading')
Katalog <h4><span class="label label-success">{{{ $category['title'] }}}</span></h4>
@endsection

@section('script')

<link rel="stylesheet" href="{{ url('/') }}/bower_resources/chosen_v1.3.0/chosen.css" />
<link rel="stylesheet" href="{{ url('/') }}/style/backend/css/chosen.css"/>
<script src="{{ url('/') }}/bower_resources/chosen_v1.3.0/chosen.jquery.js"></script>

<script>
 $('.modal').on('shown.bs.modal', function () {
   $('.chosen-select', this).chosen('destroy').chosen();
 });
</script>

@endsection

@section('breadcumb')

<li>
    <a href="{{ url('/') }}/backend">Home</a>
</li>
<li class="active">
    <a href="{{ url('/') }}/backend/category">Katalog</a>
</li>
<li class="active">
    <a href="{{ url('/') }}/backend/item/{{{ $category['id'] }}}">{{{ $category['title'] }}}</a>
</li>

@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                Daftar Katalog &nbsp;
                <a href="{{ url('/') }}/backend/category" class="btn btn-sm btn-info">
                    <i class="fa fa-arrow-left"></i> Kembali ke Kategori
                </a>
                </h3>
                <div class="box-tools pull-right">
                <button type="button"  class="btn btn-md btn-primary" data-toggle="modal" data-target="#addKatalog">
                    <i class="fa fa-plus-square"></i> Tambah Katalog
                </button>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
            <!-- will be used to show any messages -->
            @if (Session::has('message'))
                <div class="alert alert-info">
                    <i class="fa fa-info"></i> {{ Session::get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
                <table class="table table-bordered table-striped datatable">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Judul</th>
                            <th>Deskripsi</th>
                            <th width="10%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                         <?php $increment = 1 ?>
                         @foreach($item as $key => $value)
                             <tr>
                                <td>{{ $increment }}</td>
                                <td>
                                    <strong>{{{ strip_tags($value->title) }}}</strong>
                                </td>
                                <td>{{{ substr(strip_tags($value->description), 0, 100) }}} [..]</td>
                                <td>
                                    <button type="button"
                                            class="btn btn-sm btn-info btn-md"
                                            data-toggle="modal"
                                            data-target="#editKatalog{{ $value->id }}">
                                      <i class="fa fa-pencil"></i>
                                    </button>
                                    <button type="button"
                                            class="btn btn-sm btn-danger btn-md"
                                            data-toggle="modal"
                                            data-target="#deleteKatalog{{ $value->id }}">
                                      <i class="fa fa-trash-o"></i>
                                    </button>
                                </td>
                             </tr>
                         <?php $increment++ ?>
                         @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No.</th>
                            <th>Judul</th>
                            <th>Deskripsi</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

{{--Modal--}}

@foreach($item as $key => $value)
<!-- Edit Modal -->
<div class="modal modal-wide fade" id="editKatalog{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="editKatalogLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Sunting <strong>"{{{ strip_tags($value->title) }}}"</strong></h4>
      </div>

      {{ Form::open(array('route' => array('backend.item.update', $value->id), 'method' => 'PUT', 'files' => true)) }}

      <div class="modal-body">
            <label for="title">Judul:</label>
            <input type="text"
                    name="title"
                    value="{{{ strip_tags($value->title) }}}"
                    class="form-control input-lg"
                    placeholder="Contoh: Pembangunan Kota"
                    required/>
            <label for="categories_id">Kategori:</label>
            <select name="categories_id" class="form-control chosen-select">
                <option value="{{{ $value->categories_id }}}" selected>Tidak Diubah</option>
                <optgroup label="Ganti Kategori:">
                    @foreach($category_dropdown as $key => $category_edit_list)
                    <option value="{{{ $category_edit_list->id }}}">
                        {{{ $category_edit_list->id}}}. {{{ $category_edit_list->title }}}
                    </option>
                    @endforeach
                </optgroup>
            </select>
            <textarea name="description" class="tinymce" cols="30" rows="10">{{ $value->description }}</textarea>
          <label for="file">Ganti Foto Katalog (Opsional)</label>
          <input type="file"
                 name="file"
                 class="form-control input-lg"
                 accept="image/*"/>
          <label for="docs">Ganti Dokumen Katalog (Opsional)</label>
          <input type="file"
                 name="docs"
                 class="form-control input-lg"
                 accept="application/pdf"/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Batalkan</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan Perubahan</button>
      </div>

      {{ Form::close() }}

    </div>
  </div>
</div>
@endforeach

@foreach($item as $key => $delete)
<!-- Delete Modal -->
<div class="modal fade" id="deleteKatalog{{ $delete->id }}" tabindex="-1" role="dialog" aria-labelledby="editKatalogLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Hapus <strong>"{{{ strip_tags($delete->title) }}}"</strong></h4>
      </div>
      <div class="modal-body">

        <p>Anda yakin ingin menghapus <strong>"{{{ strip_tags($delete->title) }}}"</strong>?</p>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Batalkan</button>
        {{ Form::open(array('route' => array('backend.item.destroy', $delete->id), 'method' => 'DELETE')) }}
            <input name="categories_id" type="hidden" value="{{{ $delete->categories_id }}}"/>
            <button type="submit" class="btn btn-primary"><i class="fa fa-trash"></i> Hapus</button>
        {{ Form::close() }}
      </div>
    </div>
  </div>
</div>
@endforeach


{{--Add Katalog Modal--}}
<div class="modal modal-wide fade" id="addKatalog" tabindex="-1" role="dialog" aria-labelledby="addKatalogLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Katalog</h4>
      </div>

       {{ Form::open(array('method' => 'POST', 'url' => 'backend/item','files' => true)) }}

      <div class="modal-body">
            <label for="title">Judul:</label>
            <input type="text"
                    name="title"
                    class="form-control input-lg"
                    placeholder="Contoh: Pembangunan Kota"
                    required/>

                <label for="categories_id">Kategori:</label>
                <select name="categories_id" class="form-control chosen-select">
                    <option value="{{{ $category['id'] }}}" selected>{{{ $category['title'] }}}</option>
                    <optgroup label="Pilih Kategori Lainnya:">
                        @foreach($category_dropdown as $key => $category_list)
                        <option value="{{{ $category_list->id }}}">
                            {{{ $category_list->id}}}. {{{ $category_list->title }}}
                        </option>
                        @endforeach
                    </optgroup>
                </select>
            <textarea name="description" class="tinymce" cols="30" rows="10"></textarea>
            <label for="file">Gambar:</label>
            <input type="file"
                    name="file"
                    class="form-control input-lg"
                    accept="image/*"
                    required/>
            <label for="docs">Katalog (PDF):</label>
            <input type="file"
                    name="docs"
                    class="form-control input-lg"
                    accept="application/pdf"
                    required/>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Batalkan</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan Perubahan</button>
      </div>

      {{ Form::close() }}

    </div>
  </div>
</div>

@endsection
