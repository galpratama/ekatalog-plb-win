@extends('backend.layouts.default')

@section('heading')
Tautan
@endsection

@section('breadcumb')

<li>
    <a href="{{ url('/') }}/backend">Home</a>
</li>
<li class="active">
    <a href="{{ url('/') }}/backend/link">Tautan</a>
</li>

@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Daftar Tautan</h3>
                <div class="box-tools pull-right">
                <button type="button"  class="btn btn-md btn-primary" data-toggle="modal" data-target="#addTautan">
                    <i class="fa fa-plus-square"></i> Tambah Tautan
                </button>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
            <!-- will be used to show any messages -->
            @if (Session::has('message'))
                <div class="alert alert-info">
                    <i class="fa fa-info"></i> {{ Session::get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
                <table class="table table-bordered table-striped datatable">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Judul</th>
                            <th>Tautan</th>
                            <th width="8%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                         <?php $increment = 1 ?>
                         @foreach($link as $key => $value)
                             <tr>
                                <td>{{ $increment }}</td>
                                <td>
                                    <strong>{{{ strip_tags($value->title) }}}</strong>
                                </td>
                                <td>{{ $value->url }}</td>
                                <td>
                                    <button type="button"
                                            class="btn btn-sm btn-info btn-md"
                                            data-toggle="modal"
                                            data-target="#editTautan{{ $value->id }}">
                                      <i class="fa fa-pencil"></i>
                                    </button>
                                    <button type="button"
                                            class="btn btn-sm btn-danger btn-md"
                                            data-toggle="modal"
                                            data-target="#deleteTautan{{ $value->id }}">
                                      <i class="fa fa-trash-o"></i>
                                    </button>
                                </td>
                             </tr>
                         <?php $increment++ ?>
                         @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No.</th>
                            <th>Judul</th>
                            <th>Tautan</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

{{--Modal--}}

@foreach($link as $key => $value)
<!-- Edit Modal -->
<div class="modal modal-wide fade" id="editTautan{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="editTautanLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Sunting <strong>"{{{ strip_tags($value->title) }}}"</strong></h4>
      </div>

      {{ Form::open(array('route' => array('backend.link.update', $value->id), 'method' => 'PUT')) }}

      <div class="modal-body">
            <label for="title">Judul:</label>
            <input type="text"
                    name="title"
                    value="{{{ strip_tags($value->title) }}}"
                    class="form-control input-lg"
                    placeholder="Contoh: Bagaimana cara mendaftar?"
                    required=""/>
            <label for="title">Tautan Lengkap (dengan http://):</label>
            <input type="text"
                 name="url"
                 value="{{ $value->url }}"
                 class="form-control input-lg"
                 placeholder="Contoh: http://litbang.pu.go.id/"
                 required=""/>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Batalkan</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan Perubahan</button>
      </div>

      {{ Form::close() }}

    </div>
  </div>
</div>
@endforeach

@foreach($link as $key => $delete)
<!-- Delete Modal -->
<div class="modal fade" id="deleteTautan{{ $delete->id }}" tabindex="-1" role="dialog" aria-labelledby="editTautanLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Hapus <strong>"{{{ strip_tags($delete->title) }}}"</strong></h4>
      </div>
      <div class="modal-body">

        <p>Anda yakin ingin menghapus <strong>"{{{ strip_tags($delete->title) }}}"</strong>?</p>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Batalkan</button>
        {{ Form::open(array('route' => array('backend.link.destroy', $delete->id), 'method' => 'DELETE')) }}
            <button type="submit" class="btn btn-primary"><i class="fa fa-trash"></i> Hapus</button>
        {{ Form::close() }}
      </div>
    </div>
  </div>
</div>
@endforeach


{{--Add Tautan Modal--}}
<div class="modal modal-wide fade" id="addTautan" tabindex="-1" role="dialog" aria-labelledby="addTautanLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Tautan</h4>
      </div>

       {{ Form::open(array('method' => 'POST')) }}

      <div class="modal-body">
            <label for="title">Judul:</label>
            <input type="text"
                    name="title"
                    class="form-control input-lg"
                    placeholder="Contoh: Bagaimana cara mendaftar?"
                    required/>
          <label for="url">Tautan Lengkap (dengan http://):</label>
          <input type="text"
                 name="url"
                 class="form-control input-lg"
                 placeholder="Contoh: http://litbang.pu.go.id/"
                 required=""/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Batalkan</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan Perubahan</button>
      </div>

      {{ Form::close() }}

    </div>
  </div>
</div>

@endsection