@extends('backend.layouts.default')

@section('heading')
Kontak
@endsection

@section('breadcumb')

<li>
    <a href="{{ url('/') }}/backend">Home</a>
</li>
<li class="active">
    <a href="{{ url('/') }}/backend/contact">Kontak</a>
</li>

@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Daftar Kontak</h3>
                <div class="box-tools pull-right">
                </div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
            <!-- will be used to show any messages -->
            @if (Session::has('message'))
                <div class="alert alert-info">
                    <i class="fa fa-info"></i> {{ Session::get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
                <table class="table table-bordered table-striped datatable">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Pesan</th>
                            <th width="8%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                         <?php $increment = 1 ?>
                         @foreach($contact as $key => $value)
                             <tr>
                                <td>{{ $increment }}</td>
                                <td>
                                    <strong>{{ strip_tags($value->name) }}</strong>
                                </td>
                                <td>{{ substr(strip_tags($value->message), 0, 100) }} [..]</td>
                                <td>
                                    <button type="button"
                                            class="btn btn-sm btn-info btn-md"
                                            data-toggle="modal"
                                            data-target="#editContact{{ $value->id }}">
                                      <i class="fa fa-eye"></i>
                                    </button>
                                    <button type="button"
                                            class="btn btn-sm btn-danger btn-md"
                                            data-toggle="modal"
                                            data-target="#deleteContact{{ $value->id }}">
                                      <i class="fa fa-trash-o"></i>
                                    </button>
                                </td>
                             </tr>
                         <?php $increment++ ?>
                         @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Pesan</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

{{--Modal--}}

@foreach($contact as $key => $value)
<!-- View Contact Modal -->
<div class="modal modal-wide fade" id="editContact{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="editContactLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Lihat pesan dari <strong>"{{ strip_tags($value->name) }}"</strong></h4>
      </div>

      {{ Form::open(array('route' => array('backend.contact.update', $value->id), 'method' => 'PUT')) }}

      <div class="modal-body">
            <label for="title">Nama:</label>
            <input type="text"
                    name="name"
                    value="{{{ $value->name }}}"
                    class="form-control input-lg"
                    disabled />
            <label for="title">Surel:</label>
            <input type="text"
                    name="email"
                    value="{{{ $value->email }}}"
                    class="form-control input-lg"
                    disabled />
          <label for="message">Pesan:</label><br/>
          <div class="well">
          {{{ $value->message }}}
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary pull-left" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Kembali</button>
        <a href="mailto:{{{ $value->email }}}" class="btn btn-success pull-right"><i class="fa fa-envelope"></i> Kirim Balasan</a>
      </div>

      {{ Form::close() }}

    </div>
  </div>
</div>
@endforeach

@foreach($contact as $key => $delete)
<!-- Delete Modal -->
<div class="modal fade" id="deleteContact{{ $delete->id }}" tabindex="-1" role="dialog" aria-labelledby="editContactLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Hapus <strong>"{{{ $delete->name }}}"</strong></h4>
      </div>
      <div class="modal-body">

        <p>Anda yakin ingin menghapus pesan dari <strong>"{{{ $delete->name }}}"</strong>?</p>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary pull-left" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Batalkan</button>
        {{ Form::open(array('route' => array('backend.contact.destroy', $delete->id), 'method' => 'DELETE')) }}
            <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i> Hapus</button>
        {{ Form::close() }}
      </div>
    </div>
  </div>
</div>
@endforeach

@endsection