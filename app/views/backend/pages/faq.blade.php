@extends('backend.layouts.default')

@section('heading')
FAQ
@endsection

@section('breadcumb')

<li>
    <a href="{{ url('/') }}/backend">Home</a>
</li>
<li class="active">
    <a href="{{ url('/') }}/backend/faq">FAQ</a>
</li>

@endsection

@section('content')

<div class="row">
    <div class="col-xs-12">

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Daftar FAQ</h3>
                <div class="box-tools pull-right">
                <button type="button"  class="btn btn-md btn-primary" data-toggle="modal" data-target="#addFaq">
                    <i class="fa fa-plus-square"></i> Tambah FAQ
                </button>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
            <!-- will be used to show any messages -->
            @if (Session::has('message'))
                <div class="alert alert-info">
                    <i class="fa fa-info"></i> {{ Session::get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
                <table class="table table-bordered table-striped datatable">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Judul</th>
                            <th>Deskripsi</th>
                            <th width="8%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                         <?php $increment = 1 ?>
                         @foreach($faq as $key => $value)
                             <tr>
                                <td>{{ $increment }}</td>
                                <td>
                                    <strong>{{{$value->title}}}</strong>
                                </td>
                                <td>{{{ substr(strip_tags($value->description), 0, 100) }}} [..]</td>
                                <td>
                                    <button type="button"
                                            class="btn btn-sm btn-info btn-md"
                                            data-toggle="modal"
                                            data-target="#editFaq{{ $value->id }}">
                                      <i class="fa fa-pencil"></i>
                                    </button>
                                    <button type="button"
                                            class="btn btn-sm btn-danger btn-md"
                                            data-toggle="modal"
                                            data-target="#deleteFaq{{ $value->id }}">
                                      <i class="fa fa-trash-o"></i>
                                    </button>
                                </td>
                             </tr>
                         <?php $increment++ ?>
                         @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No.</th>
                            <th>Judul</th>
                            <th>Deskripsi</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

{{--Modal--}}

@foreach($faq as $key => $value)
<!-- Edit Modal -->
<div class="modal modal-wide fade" id="editFaq{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="editFaqLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Sunting <strong>"{{ $value->title }}"</strong></h4>
      </div>

      {{ Form::open(array('route' => array('backend.faq.update', $value->id), 'method' => 'PUT')) }}

      <div class="modal-body">
            <label for="title">Judul:</label>
            <input type="text"
                    name="title"
                    value="{{{ $value->title }}}"
                    class="form-control input-lg"
                    placeholder="Contoh: Bagaimana cara mendaftar?"
                    required/>

            <textarea name="description" class="tinymce" cols="30" rows="10">{{ $value->description }}</textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Batalkan</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan Perubahan</button>
      </div>

      {{ Form::close() }}

    </div>
  </div>
</div>
@endforeach

@foreach($faq as $key => $delete)
<!-- Delete Modal -->
<div class="modal fade" id="deleteFaq{{ $delete->id }}" tabindex="-1" role="dialog" aria-labelledby="editFaqLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Hapus <strong>"{{ $delete->title }}"</strong></h4>
      </div>
      <div class="modal-body">

        <p>Anda yakin ingin menghapus <strong>"{{ $delete->title }}"</strong>?</p>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Batalkan</button>
        {{ Form::open(array('route' => array('backend.faq.destroy', $delete->id), 'method' => 'DELETE')) }}
            <button type="submit" class="btn btn-primary"><i class="fa fa-trash"></i> Hapus</button>
        {{ Form::close() }}
      </div>
    </div>
  </div>
</div>
@endforeach


{{--Add FAQ Modal--}}
<div class="modal modal-wide fade" id="addFaq" tabindex="-1" role="dialog" aria-labelledby="addFaqLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah FAQ</h4>
      </div>

       {{ Form::open(array('method' => 'POST')) }}

      <div class="modal-body">
            <label for="title">Judul:</label>
            <input type="text"
                    name="title"
                    class="form-control input-lg"
                    placeholder="Contoh: Bagaimana cara mendaftar?"
                    required/>
            <textarea name="description" class="tinymce" cols="30" rows="10"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Batalkan</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan Perubahan</button>
      </div>

      {{ Form::close() }}

    </div>
  </div>
</div>

@endsection