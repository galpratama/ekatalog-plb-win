<!-- bootstrap 3.0.2 -->
<link href="{{ url('/') }}/bower_resources/adminlte/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- font Awesome -->
<link href="{{ url('/') }}/bower_resources/adminlte/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="{{ url('/') }}/bower_resources/adminlte/css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker -->
<link href="{{ url('/') }}/bower_resources/adminlte/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
<!-- bootstrap wysihtml5 - text editor -->
<link href="{{ url('/') }}/bower_resources/adminlte/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="{{ url('/') }}/bower_resources/adminlte/css/AdminLTE.css" rel="stylesheet" type="text/css" />
<!-- DATA TABLES -->
<link href="{{ url('/') }}/bower_resources/adminlte/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<!-- Admin Style -->
<link href="{{ url('/') }}/style/backend/css/admin.min.css" rel="stylesheet" type="text/css" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="{{ url('/') }}/bower_resources/html5shiv/dist/html5shiv.min.js"></script>
  <script src="{{ url('/') }}/bower_resources/respond/dest/respond.min.js"></script>
<![endif]-->