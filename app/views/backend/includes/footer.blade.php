<!-- jQuery 2.0.2 -->
<script src="{{ url('/') }}/bower_resources/jquery/dist/jquery.min.js" type="text/javascript"></script>
<!-- jQuery UI 1.10.3 -->
<script src="{{ url('/') }}/bower_resources/adminlte/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="{{ url('/') }}/bower_resources/adminlte/js/bootstrap.min.js" type="text/javascript"></script>
<!-- daterangepicker -->
<script src="{{ url('/') }}/bower_resources/adminlte/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ url('/') }}/bower_resources/adminlte/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
<!-- iCheck -->
<script src="{{ url('/') }}/bower_resources/adminlte/js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
<!-- DATA TABES SCRIPT -->
<script src="{{ url('/') }}/bower_resources/adminlte/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="{{ url('/') }}/bower_resources/adminlte/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        $(".datatable").dataTable({
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": true,
        });
    });
</script>
<!-- TinyMCE -->
<script src="{{ url('/') }}/bower_resources/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    theme: "modern",
    skin: 'light',
    selector: "textarea.tinymce",
    menubar: true,
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table paste contextmenu jbimages textcolor colorpicker"
    ],
    toolbar: "styleselect  forecolor backcolor | bold italic | alignleft aligncenter alignright alignjustify | outdent indent | bullist numlist | link image media jbimages"

 });

/**
 * this workaround makes magic happen
 * thanks @harry: http://stackoverflow.com/questions/18111582/tinymce-4-links-plugin-modal-in-not-editable
 */
$(document).on('focusin', function(e) {
    if ($(event.target).closest(".mce-window").length) {
        e.stopImmediatePropagation();
    }
});
</script>

<!-- Bootstrap Modal Wide -->
<script>
    $(".modal-wide").on("show.bs.modal", function() {
      var height = $(window).height() - 200;
      $(this).find(".modal-body").css("max-height", height);
    });
</script>


<!-- AdminLTE App -->
<script src="{{ url('/') }}/bower_resources/adminlte/js/AdminLTE/app.js" type="text/javascript"></script>

<!-- AdminLTE for demo purposes -->

<!-- Unused -->

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="/bower_resources/adminlte/js/AdminLTE/dashboard.js" type="text/javascript"></script> -->
<!-- <script src="/bower_resources/adminlte/js/AdminLTE/demo.js" type="text/javascript"></script>-->

