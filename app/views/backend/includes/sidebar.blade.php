<!-- Sidebar user panel -->
<div class="user-panel">
    <div class="pull-left image">
        <img src="{{ url('/') }}/bower_resources/adminlte/img/avatar2.png" class="img-circle" alt="User Image" />
    </div>
    <div class="pull-left info">
        <p> {{ Auth::user()->full_name }}</p>

        <a href="#"><i class="fa fa-circle text-success"></i> Terhubung</a>
    </div>
</div>
<!-- /.search form -->
<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
    <li>
        <a href="{{ url('/') }}/backend">
            <i class="fa fa-home"></i> <span>Beranda</span>
        </a>
    </li>
    <li>
        <a href="{{ url('/') }}/backend/user">
            <i class="fa fa-users"></i> <span>Pengguna</span>
        </a>
    </li>
    <li>
        <a href="{{ url('/') }}/backend/category">
            <i class="fa fa-th"></i> <span>Katalog</span>
        </a>
    </li>
    <li>
        <a href="{{ url('/') }}/backend/faq">
            <i class="fa fa-question-circle"></i> <span>FAQ</span>
        </a>
    </li>
    <li>
        <a href="{{ url('/') }}/backend/page">
            <i class="fa fa-file"></i> <span>Halaman</span>
        </a>
    </li>
    <li>
        <a href="{{ url('/') }}/backend/link">
            <i class="fa fa-chain"></i> <span>Tautan</span>
        </a>
    </li>
    <li>
        <a href="{{ url('/') }}/backend/contact">
            <i class="fa fa-envelope-o"></i> <span>Kontak</span>
        </a>
    </li>
    <li>
        <a href="{{ url('/') }}/backend/administrator">
            <i class="fa fa-shield"></i> <span>Administrator</span>
        </a>
    </li>
    <li>
        <a href="{{ url('/') }}/backend/setting">
            <i class="fa fa-gear"></i> <span>Pengaturan</span>
        </a>
    </li>
</ul>
