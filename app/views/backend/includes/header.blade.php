<a href="{{ url('/') }}/backend" class="logo">
    <!-- Add the class icon to your logo image or logo icon to add the margining -->
    E-katalog Puslitbang
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </a>
    <div class="navbar-right">
        <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <li><a href="{{ url('/') }}" target="_blank"> Ke Aplikasi &nbsp; <i class="fa fa-external-link-square"></i></a></li>
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i>
                    <span> {{ Auth::user()->full_name }} <i class="caret"></i></span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header bg-black">
                        <img src="{{ url('/') }}/bower_resources/adminlte/img/avatar2.png" class="img-circle" alt="User Image" />
                        <p>
                            {{ Auth::user()->full_name }}
                            <small>Administrator di Ekatalog Puslitbang</small>
                        </p>
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="#" class="btn btn-default btn-flat"><i class="fa fa-gear"></i></a>
                        </div>
                        <div class="pull-right">
                            <a href="{{ url('/') }}/backend/logout" class="btn btn-default btn-flat"><i class="fa fa-power-off"></i> Keluar</a>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
